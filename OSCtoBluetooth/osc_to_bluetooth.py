#
# Forward OSC messages to Bluetooth Low Energy
#
# Based on:
# Search for BLE UART devices and list all that are found.
# Author: Tony DiCola
#
# Extended by Axel Hoesl
#
# pyosc examples

# Imports
import sys
import atexit
import uuid

import time
from time import sleep

import Adafruit_BluefruitLE
from Adafruit_BluefruitLE.services import UART

from OSC import OSCServer
from OSC import OSCClient, OSCMessage

# Define service and characteristic UUIDs used by the UART service.
UART_SERVICE_UUID = uuid.UUID('6E400001-B5A3-F393-E0A9-E50E24DCCA9E')
TX_CHAR_UUID      = uuid.UUID('6E400002-B5A3-F393-E0A9-E50E24DCCA9E')
RX_CHAR_UUID      = uuid.UUID('6E400003-B5A3-F393-E0A9-E50E24DCCA9E')


# Get the BLE provider for the current platform.
ble = Adafruit_BluefruitLE.get_provider()

rx = 0
tx = 0

bt_search_delay = 1.0
bt_msg_delay = 0.01

print_bluetooth = True

# Slider
slider = None
search_slider = True
slider_connected = False

# Setup OSC Server
osc_server = OSCServer( ("localhost", 12000) )
osc_server.timeout = 0
osc_server_run = False

# Setup OSC Client
echo_osc_messages = False
osc_client_started = False
osc_client_connected = False
osc_client = OSCClient()

osc_delay = 0.02

# this method of reporting timeouts only works by convention
# that before calling handle_request() field .timed_out is
# set to False
def handle_timeout(self):
    self.timed_out = True

# funny python's way to add a method to an instance of a class
import types
osc_server.handle_timeout = types.MethodType(handle_timeout, osc_server)


# Main function implements the program logic so it can run in a background
# thread. Most platforms require the main thread to handle GUI events and other
# asyncronous events like BLE actions. All of the threading logic is taken care
# of automatically though and you just need to provide a main function that uses
# the BLE provider.
def main():
    # Clear any cached data because both bluez and CoreBluetooth have issues with
    # caching data and it going stale.
    ble.clear_cached_data()

    # Get the first available BLE network adapter and make sure it's powered on.
    adapter = ble.get_default_adapter()
    adapter.power_on()
    print('Using adapter: {0}'.format(adapter.name))

    # Start scanning with the bluetooth adapter.
    adapter.start_scan()

    # Use atexit.register to call the adapter stop_scan function before quiting.
    # This is good practice for calling cleanup code in this main function as
    # a try/finally block might not be called since this is a background thread.
    atexit.register(adapter.stop_scan)
    atexit.register(close_osc_server)

    global slider, search_slider, bt_search_delay, osc_delay

    if search_slider:

        # Prints
        print('Searching for UART device: Slider...')
        print('Press Ctrl-C to quit (will take ~30 seconds on OSX).')

        # Enter a loop and print out whenever a new UART device is found.
        known_uarts = set()

        while search_slider:
            # Call UART.find_devices to get a list of any UART devices that
            # have been found.  This call will quickly return results and does
            # not wait for devices to appear.
            found = set(UART.find_devices())

            # Check for new devices that haven't been seen yet and print out
            # their name and ID (MAC address on Linux, GUID on OSX).
            new = found - known_uarts
            for device in new:
                print('Found UART: {0} [{1}]'.format(device.name, device.id))

                # If 'Slider' is found: stop searching and connect
                if device.name == 'Slider':
                    search_slider = False
                    global slider
                    slider = device

                    connect_slider()
                    start_osc_server()

                    adapter.stop_scan()

            # update known UARTS
            known_uarts.update(new)

            # Sleep for a second and see if new devices have appeared.
            time.sleep(bt_search_delay)
    else:
        start_osc_server()
        adapter.stop_scan()

    # Enter a loop and forward OSC messages to 'Slider' via Bluetooth LE
    while osc_server_run:
        time.sleep(osc_delay)
        each_frame()

    # Exit program
    exit_programm()

def connect_slider():
    global slider, search_slider, slider_connected

    if not search_slider:
        slider.connect()
        print ('Connect to Slider')
        slider_connected = True

        #atexit.register(disconnect_slider)

        print('Discovering services...')
        slider.discover([UART_SERVICE_UUID], [TX_CHAR_UUID, RX_CHAR_UUID])


        global uart, rx, tx
        # Find the UART service and its characteristics.
        uart = slider.find_service(UART_SERVICE_UUID)
        rx = uart.find_characteristic(RX_CHAR_UUID)
        tx = uart.find_characteristic(TX_CHAR_UUID)

        rx.start_notify(receive_bluetooth)
        print('Subscribing to RX characteristic changes...')

def disconnect_slider():
    global slider, slider_connected

    if slider_connected:
       # slider.disconnect()
        print ('Disconnecting Slider')

        slider_connected = False


# Start OSC Server and attach Message handlers
def start_osc_server():
    global osc_server_run

    if not osc_server_run:
        osc_server_run = True

        osc_server.addMsgHandler( "/connect",           connect_callback )
        osc_server.addMsgHandler( "/disconnect",        disconnect_callback )
        osc_server.addMsgHandler( "/echo",              echo_callback )
        osc_server.addMsgHandler( "/test",              test_callback )

        osc_server.addMsgHandler( "/command",           command_callback )

        osc_server.addMsgHandler( "/stopserver",        stop_callback )

        print('Listening to OSC messages from Processing')

        global echo_osc_messages
        if echo_osc_messages:
            start_osc_client()

def start_osc_client():
    global osc_server_run, osc_client_started

    if osc_server_run and not osc_client_started:
        osc_client.connect( ('localhost', 10000) )
        osc_client_started = True
        print('Starting OSC Client (for echoing messages)')

def close_osc_client():
    global osc_server_run, osc_client_started, osc_client_connected, echo_osc_messages

    if osc_server_run and osc_client_started and osc_client_connected and echo_osc_messages:
        global osc_client
        osc_client.close()
        osc_client = OSCClient()

        osc_client_started = False
        print('Closing OSC Client')

def close_osc_server():
    global osc_server_run, osc_client_connected

    if osc_server_run and osc_client_connected:
        disconnect();
        osc_server_run = False
        print ('CLOSING OSC SERVER')


# Each frame handle server requests
def each_frame():
    global osc_server
    # clear timed_out flag
    osc_server.timed_out = False

    # handle all pending requests then return
    while not osc_server.timed_out:
        osc_server.handle_request()

def connect_callback(path, tags, args, source):
    global osc_server_run, osc_client_connected

    if osc_server_run and not osc_client_connected:
        osc_client_connected = True
        print('Client connected')

        global echo_osc_messages
        if echo_osc_messages:
            start_osc_client()

def disconnect_callback(path, tags, args, source):
    disconnect()

def disconnect():
    global osc_server_run, osc_client_connected, echo_osc_messages, slider_connected, bt_msg_delay

    if slider_connected:
        send_bluetooth_message("0#")
        if bt_msg_delay > 0: time.sleep (bt_msg_delay);

        send_bluetooth_message("+#")
        if bt_msg_delay > 0: time.sleep (bt_msg_delay);

        send_bluetooth_message("+#")

    if osc_server_run and osc_client_connected:
        osc_client_connected = False
        print('Client disconnected')
        close_osc_client()

def echo_callback(path, tags, args, source):

    if len(args) > 0:
        global osc_server_run, osc_client_connected, echo_osc_messages

        if osc_server_run and osc_client_connected and not osc_client_started:
            start_osc_client()

        if osc_server_run and osc_client_started and osc_client_connected:

            if args[0] == 1.0:   echo_osc_messages = True
            elif args[0] == 0.0: echo_osc_messages = False

            print ('echo OSC messages: %s' % echo_osc_messages)


# Test callback
def test_callback(path, tags, args, source):
    global osc_server_run, osc_client_connected

    if osc_server_run and osc_client_connected:
        print ('Forwarding OSC Message to Slider via Bluetooth')

        global echo_osc_messages
        if echo_osc_messages:
            msg = OSCMessage("/test")

            for arg in args:
                 msg.append(arg)

            osc_client.send(msg)
            print("echoing message")

        for arg in args:
                print (arg)

def command_callback(path, tags, args, source):
    global osc_server_run, osc_client_connected, search_slider, print_bluetooth, bt_msg_delay

    if len(args) > 0:
        for arg in args:
            command = arg

            if osc_server_run and osc_client_connected and slider_connected:
                send_bluetooth_message(command)

                if bt_msg_delay > 0: time.sleep(bt_msg_delay);

            #if print_bluetooth:
            #   print ('Sending via BLE: %s' % command)



# Quit callback function
def stop_callback(path, tags, args, source):

    global osc_server_run, osc_client_connected, echo_osc_messages

    if osc_server_run and osc_client_connected and echo_osc_messages:
        close_osc_client()

    if osc_server_run:
        close_osc_server()

def send_bluetooth_message(msg):
    global tx, print_bluetooth, slider_connected

    if slider_connected:
        tx.write_value(msg)

        if print_bluetooth:
            print ('Sending via BLE: %s' % msg)

def receive_bluetooth(data):
    global print_bluetooth

    if print_bluetooth:
        print(data)

def exit_programm():
    print ('Closing programm')
    exit()
    sys.exit(0)

# Initialize the BLE system.  MUST be called before other BLE calls!
ble.initialize()

# Start the mainloop to process BLE events, and run the provided function in
# a background thread.  When the provided main function stops running, returns
# an integer status code, or throws an error the program will exit.
ble.run_mainloop_with(main)
