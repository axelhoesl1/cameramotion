
// Initialize system parameters
void initialize() {
  // print prompt
  if(DEBUG_OUTPUT) ble_printf("\n--- Initialization --- \n");
  initialized = false;
  
  // overwrite initialization
  overwriteInitialization();
}

// Overwrite initialization parameters
void overwriteInitialization() {
    if(DEBUG_OUTPUT) ble_printf("\n--- Overwriting Initalization ---\n");

    // set parameters
    currentMicrostepping = sercom[1];
    currentCamera = sercom[2];

    // save and print initialization
    saveInitializationToEEPROM();
    printInitialization();
    initialized = true;
}

// Programmed Drive
void programmedDrive() {
  // if the pointers run into NULL fields of array, terminate the drive
  if (*nextPoint == NULL && *nextSpeed == NULL && *nextAcc == NULL) {
    printStepperParameter();
    currentPositionSaved = false;
    if(DEBUG_OUTPUT) ble_printf("[+] Cruise finished, visited points: %d\n", point_counter);
    point_counter = 0;
    state = IDLE_STATE;
  }
  // set the pointers to new waypoint and instruct stepper 
  else {
    slideTo(*nextSpeed,*nextAcc,*nextPoint*currentMicrostepping);
    point_counter++;
    printWaypointParameter();
    nextPoint += 3;
    nextSpeed += 3;
    nextAcc += 3;
  }
}

// Direct Drive
void directDrive() {
    if(DEBUG_OUTPUT) ble_printf("\n--- DirectDrive --- \n");

    // set runmode, parser, speed and acceleration
    select_runmode = &AccelStepper::run;
    select_parser = &directDriveParser;
    setSpeedAndAcc(ddSpeed,ddAcc);
}

void shootMoveShoot() {
    if(DEBUG_OUTPUT) ble_printf("\n--- Shoot Move Shoot --- \n");

    // set runmode
    select_runmode = &AccelStepper::run;

    // read parameters
    int smsStartPosRead         = sercom[1];
    int smsEndPosRead           = sercom[2];
    int smsIntervallRead        = sercom[3];
    int smsPictureIntervallRead = sercom[4];

    // TODO ERROR CHECKING

    if(DEBUG_OUTPUT) {
      ble_printf("\n     smsStartPos: %d, ", smsStartPosRead);
      ble_printf("     smsEndPos: %d\n", smsEndPosRead);
      ble_printf("     smsIntervallPos: %d, ", smsIntervallRead);
      ble_printf("     smsPictureIntervall: %d\n", smsPictureIntervallRead);
    } 

    // set global parameters
    smsStartPos = smsStartPosRead*currentMicrostepping;
    smsEndPos = smsEndPosRead*currentMicrostepping;
    smsIntervall = smsIntervallRead*currentMicrostepping;
    smsPictureIntervall = smsPictureIntervallRead;

    doSMS = true;
    smsStart = true;
}

void settings() {
  if(DEBUG_OUTPUT) ble_printf("\n--- Settings --- \n");
     // set parser
    select_parser = &settingsParser;
}

