
// Assignment of interrupts to bounce mode (change slider direction)
void switchesBounceMode() {
  attachInterrupt(SW_MOTOR_PIN, bounce_on_side, FALLING); 
  attachInterrupt(SW_END_PIN,   bounce_on_side, FALLING);  
}

// Assignment of interrupts to error mode (stop slider and move it back a bit)
void switchesProtectionMode() {
  attachInterrupt(SW_MOTOR_PIN, error_motor_side, FALLING); 
  attachInterrupt(SW_END_PIN,   error_end_side, FALLING);  
}

// Assignment of interrupts to calibration mode (change slider direction and determine rail length)
void switchesCalibrationMode() {
  attachInterrupt(SW_MOTOR_PIN, calib_motor_side, FALLING); 
  attachInterrupt(SW_END_PIN,     calib_end_side, FALLING);  
}

// Change slider direction when hitting a limit switch
void bounce_on_side() {
  // determine time since last interrupt
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  long timeDelta = interrupt_time-last_interrupt_time;

  // change slider direction
  if ((timeDelta) > 200) stepper.setSpeed(-(stepper.speed())); 

  // update last interrupt time
  last_interrupt_time = interrupt_time;
}

// Handling hitting a limit switch on the motor side
void error_motor_side() {  
   if(initialized && calibrated) {
     // update saved position
     currentPositionSaved = false;

     // mark as interrupted
     interrupted = true;

     // update calibration
     maxPos = stepper.currentPosition()-(SWITCH_SAFETY_DIST*currentMicrostepping);
     stepper.setCurrentPosition(maxPos+(SWITCH_SAFETY_DIST*currentMicrostepping));

     // set acceleration
     stepper.setAcceleration(4000*currentMicrostepping);

     // move back
     stepper.move(-2*SWITCH_SAFETY_DIST*currentMicrostepping);  
   }
}

// Handling a hitting a limit switch on the end side
void error_end_side() {  
  if(initialized && calibrated) {
    // update saved position
    currentPositionSaved = false;

    // mark as interrupted
    interrupted = true; 

    // update calibration
    stepper.setCurrentPosition(-SWITCH_SAFETY_DIST*currentMicrostepping);

    // set acceleration
    stepper.setAcceleration(4000*currentMicrostepping);

    // move back
    stepper.move(2*SWITCH_SAFETY_DIST*currentMicrostepping); 
  }
}

// Handling hitting a limit switch on the motor side in calibration mode
void calib_motor_side() {
  // determine time since last interrupt
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  long timeDelta = interrupt_time - last_interrupt_time;
  
  if (timeDelta > 200) {
    // update calibration
    maxPos = stepper.currentPosition() - SWITCH_SAFETY_DIST*currentMicrostepping;

    // set speed and accerleration
    setSpeedAndAcc(MAX_CALIB_SPEED,getAccelerationFromSpeed(MAX_CALIB_SPEED));

    // move to center of the rail
    stepper.moveTo(((maxPos+minPos)/2) - 2*SWITCH_SAFETY_DIST);
    stepper.runSpeedToPosition();

    // update calibration
    maxPos += -minPos;
    minPos = 0;
  }

  // update last interrupt time
  last_interrupt_time = interrupt_time;
}

// Handling hitting a limit switch on the end side in calibration mode
void calib_end_side() {
  // determine time since last interrupt
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  long timeDelta = interrupt_time - last_interrupt_time;

  if (timeDelta > 200) {
    // update calibration
    minPos = stepper.currentPosition() + SWITCH_SAFETY_DIST*currentMicrostepping;

    // move to the motor side
    stepper.moveTo(10000*currentMicrostepping);
  }

  // update last interrupt time
  last_interrupt_time = interrupt_time;
}
