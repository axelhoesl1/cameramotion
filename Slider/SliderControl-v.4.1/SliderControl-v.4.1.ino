/* 
 *  ############################################

    Slider Control v4.1
    
    features:
    - Direct Drive (left / right)
    - Ramping (automatic and manual acceleration/deceleration)
    - Smooth Looping (direction change before an end switch is hit)
    - Immediate and delayed speed changes (when looping: speed changes after direction change)
    - Recalibration when hitting an end-switch
    - EEPROM storage of calibration and slider position (automatically updated when stopped)
    - Shortcuts (quick access to 5 pre-defined positions e.g. the ends or the center of the rail)
    - Flip sides (adjust left and right depending on where you stand)
    - Vertical mode (when looping: drive faster in one direction, e.g. when only recording shots from top to bottom)
    - Time-Lapse
    - Remote Control via Bluetooth LE
    
    authors: 
    Axel Hoesl, Patrick Moerwald
    
    contact:
    mail: axel.hoesl@ifi.lmu.de
    web:  www.medien.ifi.lmu.de/team/axel.hoesl

* ############################################
*/ 

/* 
 * ############################################
 * MODES AND COMMANDS:
 * 
 * 
 * 1# Direct-Drive:
 * ----------------------------------------------------------------------------
 * c# calibrate
 * a#: start moving in 1st direction (button down), d#: start moving in 2nd direction (button down) 0#: stop moving (button up), 
 * XXX#: set speed XXX, i# switch immediate speed change on/off, 
 * l#: switch looping on/off, 
 * v# switch vertical mode on/off (go back fast in one direction), b# flip direction in vertical mode
 * f#: flip end positions
 * m#: switch debug messages on/off, x#: reset, +#: exit
 * 
 * 
 * 2# Programmed-Drive:
 * ----------------------------------------------------------------------------
 * Syntax: 4,TARGETPOSITION, SPEED, ACCELERATION#, 
 * Example: 2,0,150,150# (move to minPos with speed and acceleration 150), multiple waypoints are allowed
 * 
 * 
 * 3# Shoot-Move-Shoot-Drive:
 * ----------------------------------------------------------------------------
 * Syntax: 3, STARTPOS, ENDPOS, STEPS BETWEEN TWO POINTS, TIME BETWEEN TWO PICTURES (ms)#
 * 
 * 
 * 9# Settings:
 * ----------------------------------------------------------------------------
 * L#: load calibration from EEPROM, S#: save Calibration to EEPROM, E#: erase EEPROM, P#: print calibration, 
 * I# delete initialization, XX,YY#: overwrite initialization with XX for microstepping and YY for camera type,  
 * M#: switch debug messages on/off, +#: exit
 * 
 * ############################################
 */

// BLE module is attached via SPI
#include <SPI.h> 
#include <boards.h>

/*
  ! Modified libraries for the Adafruit BLE Module on Arduino Due!
*/
// Modified version of Red Bear Labs BLE library to work with Adafruit nrF8001 module
#include <RBL_nRF8001.h>

// Library for the stepper motor acceleration
#include <AccelStepper.h> 

// Needed for the vargs in BLE communication
#include <stdarg.h> 

//Needed for format strings in BLE communication
#include <stdio.h>

// Camera infra-red control features
#include <multiCameraIrControl.h> 

// For writing and reading EEPROM values
#include <Wire.h>
#include <I2CEEPROM.h>

// Some Math
#include <math.h>


/* 
 * ############################################
 * Debug output, Float precision, Bluetooth Name
 * ############################################
 */

// Print debug messages
boolean DEBUG_OUTPUT = true; 

// Print error messages
boolean ERROR_OUTPUT = true; 

// Precision in digits of fractional part in floats
#define FLOAT_PRECISION 2 

 // Define Bluetooth Low Energy Shield name (maximum length: 10 characters)
#define BLE_NAME "Slider"

/* 
 * ############################################
 * Pin definitions
 * ############################################
 */

// Pins for Adafruit nRF8001 BLE module
#define BLE_REQUEST_PIN 6
#define BLE_READY_PIN 5
#define BLE_RESET_PIN 7 

// Pins for stepper motor
#define ENA_PIN 9
#define DIR_PIN 10
#define PUL_PIN 11

// Pins for limit switches
#define SW_MOTOR_PIN 3
#define SW_END_PIN 2

// Pins for triggering infra-red LEDs
#define IR_PIN 39


/* 
 * #########################
 *  Physical Rating Definitions 
 * #########################
 */

// The maximum operational speed of the motor
#define MAX_RUN_SPEED 1200

// The maximum calibration speed of the motor
#define MAX_CALIB_SPEED 370

// Default Speed in general
#define DEFAULT_SPEED 250
#define DEFAULT_ACC 400

// Default Speed for shortcuts
#define SHORTCUT_SPEED 1000
#define SHORTCUT_ACC 650

// This is a safety margin, a small distance between the end switch and where the motor actually stops.
#define SWITCH_SAFETY_DIST 30

#define END_THRESHOLD 250

// Interrupt flag
boolean interrupted = false;


/* 
 * ############################################
 * EEPROM definitions
 * ############################################
 */

 // Field lenght and start
 #define EEPROM_FIELD_LENGTH 4
 #define EEPROM_LAST_FIELD 8

 // Stored information and location
 #define EEPROM_MICROSTEPPING 2
 #define EEPROM_CAMERA 3
 #define EEPROM_MAXPOS 4
 #define EEPROM_MINPOS 5
 #define EEPROM_POS 6
 #define EEPROM_SPD 7
 #define EEPROM_ACC 8
 
// Flag to save position on stop
boolean currentPositionSaved = true;


/* 
 * ############################################
 * STEPPER MOTOR
 * ############################################
 */
 
// Stepper Motor
AccelStepper stepper(1, PUL_PIN, DIR_PIN); 

//Function pointer to quickly switch between the different motion functions
//AccelStepper::runSpeed() and AccelStepper::run().
boolean (AccelStepper::* select_runmode)(void) = NULL;


/* 
 * ############################################
 * PARSER
 * ############################################
 */
 
// Function pointer to "Load" one of the parsers
void (* select_parser)(void) = NULL;

// For command parser
char     inData[200] = {}; 
uint32_t datalen = 0;
int32_t  sercom[200] = {};
boolean  startSwitch = false;

// For programmed drive parser
uint8_t point_counter;
long *  nextPoint;
long *  nextSpeed;
long *  nextAcc;

#define IDLE_STATE 0x00
#define DIRECT_DRIVE 0x01
#define PROGRAMMED_DRIVE 0x02
#define SHOOT_MOVE_SHOOT 0x03
#define SETTINGS 0x09

volatile uint8_t state = IDLE_STATE;


/* 
 * ############################################
 * Initialization and Calibration
 * ############################################
 */
 
// Initialization
boolean AUTOLOAD_INIT = true;
boolean initialized = false;
uint8_t currentMicrostepping = 16; 

// Calibration 
boolean AUTOLOAD_CALIB = true;
boolean calibrated = false;
int32_t maxPos = 0;
int32_t minPos = 0;

// Define number of knwown cameras for infra-red control
#define NUM_CAMS 6

static char * enum_cams [NUM_CAMS] = {
  "Nikon", "Canon", "Sony", "Olympus", "Pentax", "Minolta"
};

// Initialize cam objects for infra-red remote control
Nikon    nikon(IR_PIN);    // (0 in enum_cams)
Canon    canon(IR_PIN);    // (1 in enum_cams)
Sony     sony(IR_PIN);     // (2 in enum_cams)
Olympus  olympus(IR_PIN);  // (3 in enum_cams)
Pentax   pentax(IR_PIN);   // (4 in enum_cams)
Minolta  minolta(IR_PIN);  // (5 in enum_cams)

// Set current camera to Canon
uint8_t currentCamera = 1; 

// Direct drive mode
boolean ddExit = false;
boolean ddSpeedInit = false;
boolean ddPingPong = false;
boolean maxPoint = true;
boolean minPoint = false;
boolean ddIsLooping = false;
boolean ddIsMovingToShortcut = false;
boolean ddImmediateSpeedChange = false;
boolean ddFlip = false;
boolean ddVertical = false;
boolean ddVerticalFlip = false;
int     ddSpeed = 0;
int     ddAcc = DEFAULT_ACC;
int32_t ddTarget = 0;

// Shoot-Move-Shoot mode for time-lapse recordings
boolean smsDebug = false;
int     smsSpeed = 500;
int     smsAcc   = 500;

boolean doSMS = false;
boolean smsStart = false;
boolean smsRun = false;
int     smsStartPos = 0;
int     smsEndPos = 0;
int     smsPreviousMillis = 0;
int     smsPictureIntervall = 0;
int     smsIntervall = 0;
int     smsPicturesTaken = 0;
int     smsDeshakeDelay = 250;
int     smsShutterDelay = 200; // safe until 1/5 sec exposure time
int     smsLastRelease = 0;

// Settings
boolean settingsExit = false;

void setup() {
  /* ####################################################################### 
   *  Bluetooth Low Energy and Serial communication
   * #######################################################################
   */
  // Setting up a two-way communication over BLE, used for state commands and motion control data
  ble_start(BLE_REQUEST_PIN, BLE_READY_PIN, BLE_RESET_PIN);

  // Setting up Serial communication with a baud-rate of 57600
  Serial.begin(57600); 

  /* #######################################################################
   *  Interrupts and stepper motor
   * #######################################################################
   */

  // Assigning interrupts to the end switches, protection against collision
  // (Better before motor init to have smooth start!)
  switchesProtectionMode();
  
  // Use this pin to enable or disable current flow in stepper motor
  stepper.setEnablePin(ENA_PIN);
  
  // Invert signals (common-anode driver configuration, change if using common cathode)
  stepper.setPinsInverted(1,1,1); 

  // Set the maximum speed limit of the stepper motor
  stepper.setMaxSpeed(MAX_RUN_SPEED*currentMicrostepping);

  // Set the acceleration 
  stepper.setAcceleration(0);

  // Set initial stepper speed to 0
  stepper.setSpeed(0);

  // Enable stepper outputs
  stepper.enableOutputs();


  /* #######################################################################
   *  Initialize EEPROM and load calibration data
   * #######################################################################
   */

  // Print start prompt
  printStartPrompt();

  // Automatically initalize system with saved data
  if(AUTOLOAD_INIT) loadInitializationFromEEPROM();

  // Automatically load calibration from saved data
  if(initialized && AUTOLOAD_CALIB) loadCalibrationFromEEPROM();
  // if no saved data is available print prompt asking for calibration 
  else if(initialized && !calibrated) printCalibrationPrompt();

  /* #######################################################################
   *  Parser and run mode
   * #######################################################################
   */

   // Use run mode by default
  select_runmode = &AccelStepper::run; 

  // Select parser depending on init state
  if(!initialized) { 
    state = IDLE_STATE;
    select_parser = &standardCommandParser;  
  }
  else if (initialized && calibrated) { 
    state = DIRECT_DRIVE;
    select_parser = &directDriveParser; 
   // printDDMode();
  }

  
  /* #######################################################################
   *  Load camera data
   * #######################################################################
   */
   
  /*
    TODO: Check if this necessary! 
   
    Now we could load camera type and microstepping from the HID device,
    if we choose to store user data on HID.

  while(currentCamera > NUM_CAMS -1 || !currentMicrostepping) {
    ble_do_events();
    if (ble_available()) ble_get_command(); 
  } 
  */
}


/*
 * ######################################################
 *  Custom functions for argument printing over BLE.
 * ######################################################
 */

// Note: Arduinos printf implementation cannot handle float (%f etc.),
// use ble_print_float instead!
void ble_printf(char *pcFormat, ...) {
  if(DEBUG_OUTPUT) {
    char  cBuf[256];
    char * curr =cBuf;
    va_list list;
    va_start(list,pcFormat);
    vsnprintf(cBuf,256,pcFormat,list);
    while (*curr != NULL) ble_write(*curr++);
    va_end (list);
  }
}

// Helper function to print float
// example: ble_print_float( 3.1415, 2); 
// prints 3.14 (two decimal places)
void ble_print_float(double val, int8_t precision) {
  if(val < 0.0) {
    ble_printf("-");
    val = -val;
  }

  // prints the int part
  ble_printf ("%d",int32_t(val));  
  
  if(precision > 0) {
    // print the decimal point
    ble_printf("."); 
    unsigned long frac;
    unsigned long mult = 1;
    int8_t padding = precision -1;
    while(precision--) mult *=10;
    if(val >= 0) frac = (val - int(val)) * mult;
    else frac = (int(val)- val ) * mult;
    unsigned long frac1 = frac;
    while( frac1 /= 10 ) padding--;
    while(  padding--)
    ble_printf("0");
    ble_printf("%d",frac) ;
  }
}

// Read received data and select parser
void ble_get_command() { 
  while (ble_available()) inData[datalen++] = ble_read();
  if (inData[datalen-1] == '#') {
    select_parser();
    while (datalen > 0) inData[--datalen] = NULL;
    datalen = 0;
  }
}

// Startup procedure for Bluetooth Low Energy module
void ble_start(uint8_t reqn, uint8_t rdyn, uint8_t rst) {
  // Default pins set to 6 and 5 for REQN and RDYN
  // Set your REQN and RDYN here before ble_begin() if you need
  ble_set_pins(reqn, rdyn);

  // Default reset pin is set to 7 (set custom reset pin before ble_begin())
  ble_set_reset_pin(rst);

  // Set BLE Shield name
  ble_set_name(BLE_NAME);
  
  // Initalize and start BLE library
  ble_begin();
}

/*
 * ######################################################
 *  Main loop for receiving commands and driving the motor
 * ######################################################
 */
void loop()
{
  // Receiving commands via Bluetooth Low Energy 
  if (ble_available()) {
    ble_get_command();
  }
  ble_do_events();

  // Looping the slider
  pingPongLoop();

  // Shoot-Move-Shoot 
  shootMoveShootLoop();
 
  // When there are no steps due on either AccelStepper.run() or AccelStepper.runSpeed(),
  // we trigger the exit condition
  if (!((stepper.*select_runmode)())) {
    // save current position
    if(initialized && calibrated && !currentPositionSaved) {
        saveCurrentPositionToEEPROM();
    }
    // on exit
    exitCondition();    
  }

}

