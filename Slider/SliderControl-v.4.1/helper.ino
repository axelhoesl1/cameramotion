
// Check if given points are within the rail length
boolean checkPoints(){

  uint8_t points_legal = 0x00;
  boolean return_value = false;

  // Pointers to parameters of next way point 
  // Initialize at first way point
  nextPoint = &sercom[1];
  nextSpeed = &sercom[2];
  nextAcc = &sercom[3];

  // check each point
  while(true) {

    // exit after last point
    if (*nextPoint == NULL && *nextSpeed == NULL && *nextAcc == NULL) break;

    // position is illegal
    if (*nextPoint > maxPos/currentMicrostepping || *nextPoint < minPos/currentMicrostepping) points_legal |= 0x01;

    // speed is illegal
    if (*nextSpeed < 0 || *nextSpeed > MAX_RUN_SPEED) points_legal |= 0x10;

    nextPoint += 3;
    nextSpeed += 3;
    nextAcc += 3;
  }

  // reset to first waypoint
  nextPoint = &sercom[1];
  nextSpeed = &sercom[2];
  nextAcc = &sercom[3];

  // determine result
  if (points_legal == 0x00){
    if(DEBUG_OUTPUT) ble_printf("\n[+] Waypoints ok.");
    return_value = true;
  }
  if ((points_legal & 0x01) == 0x01){
    if(ERROR_OUTPUT) ble_printf("\n[+] Waypoints position illegal. Check parameters.");
  }

  if ((points_legal & 0x10) == 0x10){
    if(ERROR_OUTPUT) ble_printf("\n[+] Waypoints speed illegal. Check parameters.");
  }

  // return result
  return return_value;
}

// Helper to get acceleration from speed
int getAccelerationFromSpeed(int spd) {

  // 0.65 seems to be a sweet spot for acceleration (at low to medium speeds)
  float accFactor = 0.65;

  // determine acceleration
  float acc = abs(spd) * accFactor;
  int res = (int) acc;

  // Ensure that acceleration is minimum 10 (else the slider might not move or extremly slow)
  if(res < 10) res = 10;

  // Ensure that accerlartion is maximum MAX_RUN_SPEED
  if(res > MAX_RUN_SPEED) res = MAX_RUN_SPEED;

  // Return result
  return res;
}

// Helper to set speed and acceleration
 void setSpeedAndAcc(int spd, int acc) {
    // Setting maximum speed to chosen speed helps with smooth motion
    stepper.setMaxSpeed(abs(spd)*currentMicrostepping);

    // Setting speed
    stepper.setSpeed(spd*currentMicrostepping);

    // Setting acceleration
    stepper.setAcceleration(acc*currentMicrostepping);
 }

// Helper for moving to target position (with acceleration and deceleration)
 void slideTo(int spd, int acc, int target) {
    if(initialized) {
      // When in calibration mode do not check for boundaries
      if(!calibrated) {
        setSpeedAndAcc(spd,acc);
        stepper.moveTo(target);
        if(state == DIRECT_DRIVE) ddTarget = target;
      }
      // Ensure that the target is within minPos and maxPos
      else {
        if(minPos <= target && target <= maxPos) {
          setSpeedAndAcc(spd,acc);
          stepper.moveTo(target);   
          if(state == DIRECT_DRIVE) ddTarget = target;
        }
        else {
          // Print error message
          if(ERROR_OUTPUT) ble_printf("\n Target position is out of bounds! \n");
        }
      }
    }
 }
 
 // Helper for moving to target position (only with deceleration)
 // (use when slider is already in motion)
 void runTo(int spd, int acc, int target) {
    if(initialized) {
      // When in calibration mode do not check for boundaries
      if(!calibrated) {
        setSpeedAndAcc(spd,acc);
        stepper.runToNewPosition(target);
      }
      else {
        // Ensure that the target is within minPos and maxPos
        if(minPos <= target && target <= maxPos) {
          setSpeedAndAcc(spd,acc);
          stepper.runToNewPosition(target);
        }
        else {
          // Print error message
          if(ERROR_OUTPUT) ble_printf("\n Target position is out of bounds! \n");
        }
      }
    }
 }

// Helper for changing the speed of the slider
void changeSpeedImmediately(int spd) {
  if(initialized && calibrated) {
    // check if there is enough space ahead of the end before changing the speed
    if(abs(stepper.distanceToGo()) >= END_THRESHOLD*currentMicrostepping) {
      
      // constant acceleration in this case, because runSpeedToPosition() only implements decelerations (not accelerations)
      if (state == DIRECT_DRIVE) ddAcc = DEFAULT_ACC;

      // set speed and move to target position
      setSpeedAndAcc(spd,DEFAULT_ACC);
      stepper.runSpeedToPosition();
    }
  }
 }

// Helper for changing the speed of the slider
void changeSpeedImmediately(int spd, int acc) {
  if(initialized && calibrated) {
     // check if there is enough space ahead of the end before changing the speed
    if(abs(stepper.distanceToGo()) >= END_THRESHOLD*currentMicrostepping) {
      
      // constant acceleration in this case, because runSpeedToPosition() only implements decelerations (not accelerations)
      if (state == DIRECT_DRIVE) ddAcc = acc;

      // set speed and move to target position
      setSpeedAndAcc(spd,acc);
      stepper.runSpeedToPosition();
    }
  }
 }

// Helper for immediate stop without deceleration
 void hardStop() {
    slideTo(0,0,stepper.currentPosition());
 }

// Helper for stop with deceleration
 void smoothStop() {
   stepper.stop();
 }

// Helper for resetting looping state
 void resetPingPong() {
    ddPingPong = false;
    maxPoint = true;
    minPoint = false;
 }

// Helper for triggering a shutter release in time-lapse mode
 boolean releaseShutter() {
  if (DEBUG_OUTPUT) ble_printf("\nRelease shutter");

  // Determine delta between now and last release
  if(smsLastRelease != 0) {
    int smsDelta = (millis()-smsLastRelease)-smsPictureIntervall;
    if(DEBUG_OUTPUT) ble_printf("d: %d\n",smsDelta);
  }
  smsLastRelease = millis();

  // Trigger shutter release
  switch (currentCamera) {
    case 0: // Nikon
      nikon.shutterNow();
      break;
  
    case 1: // Canon
      canon.shutterNow();
      break;
  
    case 2: // Sony
      sony.shutterNow();
      break;
  
    case 3: // Olympus
      olympus.shutterNow();
      break;
  
    case 4: // Pentax
      pentax.shutterNow();
      break;
  
    case 5: // Minolta
      minolta.shutterNow();
      break;
  }

  // Count pictures taken
  smsPicturesTaken++;

  // Print debug message
  if(currentCamera < NUM_CAMS) {
    if(DEBUG_OUTPUT) ble_printf("\n shutterNow()");  
  }
}

void switchDebug() {
  // switch debug messages on/off
    DEBUG_OUTPUT = !DEBUG_OUTPUT;

    // print debug message
    ble_printf("\n Debug mode: %d\n", DEBUG_OUTPUT);
}
