
// The standard command parser
void standardCommandParser()
{
  // input data
  char *p = inData;
  char *str;

  // delimiters
  const char delimiters[] = "#,";
  uint32_t counter = 0;
  uint32_t arraysize = sizeof(sercom) / sizeof(int32_t);

  // reset array for parsed data
  for (uint32_t i = 0; i < arraysize; i++) {
    sercom[i] = NULL;
  }

  // fill array with sliced data
  while ((str = strtok_r(p, delimiters , &p)) != NULL) {
    sercom[counter++] = strtol(str, NULL, NULL);
  }

  // print array with the parsed data
  //for (int i = 0; i < zaehler; ble_printf("%d",sercom[i++]));

  // choose mode depending on parsed data
  entryCondition();
}

// Direct Drive parser
void directDriveParser()
{
  // get command
  char *p = inData;

  // stop and exit
  if (*p == '+')
  {
    // stop smoothly
    smoothStop();
    
    // exit via exitCondition
    ddExit = true;
  }
  
  // debug messages
  else if (*p == 'm') {
    switchDebug();
  }
  
  // reset
  else if (*p == 'x') {
    // reset exit
    ddExit = false;

    // reset debug
    DEBUG_OUTPUT = false;

    // reset immediate speed change
    ddImmediateSpeedChange = false;

    // reset looping
    ddPingPong = false;
    ddIsLooping = false;
    maxPoint = false;
    minPoint = false;

    // reset speed and acceleration
    ddSpeed = DEFAULT_SPEED;
    ddAcc = DEFAULT_ACC;

    // reset flipped and vertical modes
    ddFlip = false;
    ddVertical = false;
    ddVerticalFlip = false;

    // reset shortcuts
    ddIsMovingToShortcut = false;

    // stop
    hardStop();

    // print message
    ble_printf("\n DirectDrive reset \n");
  }
  
  // calibration
  else if (*p == 'c') {

    // set as uncalibrated
    calibrated = false;

    // set limit switches to calibration mode
    switchesCalibrationMode();

    // got to one end until limit switch is hit
    slideTo(MAX_CALIB_SPEED,55000,-10000*currentMicrostepping);

    // print debug message
    if(DEBUG_OUTPUT) ble_printf("\n Calibration started \n");
  }
  
  // immediate speed change
  else if (*p == 'i') {
    // switch immediate speed change on/off
    ddImmediateSpeedChange = !ddImmediateSpeedChange;

   // print debug message
    if(DEBUG_OUTPUT) ble_printf("\n Immediate Speed Change: %d\n", ddImmediateSpeedChange);
  }
  
  // move left or right
  else if (*p == 'a' || *p == 'd'){

    // save position when stopping
    currentPositionSaved = false;

    // continue looping if interrupted (e.g. via shortcut)
    if(ddIsLooping && !ddPingPong) ddPingPong = true;

    // set init speed if necessary
    if(stepper.speed() == 0 && !ddSpeedInit) { ddSpeed = DEFAULT_SPEED; ddSpeedInit = true; }

    // move left
    if (*p == 'a'){
      // set target position
      ddTarget = (ddFlip) ? minPos : maxPos;

      // setup looping
      if(ddPingPong) {
        if(ddFlip) minPoint = true;
        else maxPoint = true; 
      }    
    }
    // move right
    else if (*p == 'd'){   
      // set target position
      ddTarget = (ddFlip) ? maxPos : minPos;

      // setup looping
      if(ddPingPong) {
        if(ddFlip) maxPoint = true;
        else minPoint = true; 
      }
    }

     // go to target 
     // use acceleration+deceleration when stopped
     if(stepper.currentPosition() == minPos || stepper.currentPosition() == maxPos  || stepper.speed() == 0) slideTo(ddSpeed,ddAcc,ddTarget); 
     // just use deceleration when already running (else a hard stop occurs)
     else stepper.moveTo(ddTarget);

     // print debug message
     if(DEBUG_OUTPUT) ble_printf("\n Move to: %d\n", ddTarget/currentMicrostepping);
  }

  // smooth stop
  else if (*p == '0'){

    // save position when stopping
    currentPositionSaved = false;

    // stop smoothly
    ddTarget = 0;
    stepper.stop();

    // prevent looping from overwriting the stop
    if(ddPingPong) {
      maxPoint = false;
      minPoint = false; 
    }

    // print debug message
    if(DEBUG_OUTPUT) ble_printf("\n stop \n");
  }

  // flip the end positions (e.g. necessary when standing on the other side of the slider)
  else if (*p == 'f') {
    // switch flipping on/off
    ddFlip = !ddFlip;

    // print debug message
    if(DEBUG_OUTPUT) ble_printf("\n DirectDrive flipped: %d\n", ddFlip);
  }

  // vertical mode (go back fast in one direction when looping, reasonable in vertical slider setup as sometimes shaking occurs in one direction)
  else if (*p == 'v') {
    // switch vertical mode on/off
    ddVertical = !ddVertical;

    // print debug message
    if(DEBUG_OUTPUT) ble_printf("\n Vertical mode: %d\n", ddVertical);
  }

  // vertical mode flipped: change which direction is gone back to fast
   else if (*p == 'b') {
    // switch vertical mode flip on/off
    ddVerticalFlip = !ddVerticalFlip;

    // print debug message
    if(DEBUG_OUTPUT) ble_printf("\n Vertical mode flipped: %d\n", ddVerticalFlip);
  }

  // looping
  else if (*p == 'l'){
    // switch looping on when already in motion
    if(!ddPingPong && abs(stepper.speed()) > 0) {
       maxPoint = true;
       minPoint = true;
       ddPingPong = true;
       ddIsLooping = true;
    }
    // switch looping on when stopped
    else if (!ddPingPong && stepper.speed() == 0) {
        maxPoint = false;
        minPoint = false;
        ddPingPong = true;
        ddIsLooping = true;
    }
    // stop looping
    else {
      resetPingPong();
      ddIsLooping = false;
    }

    // print debug message
    if(DEBUG_OUTPUT) ble_printf("\n Looping: %d\n", ddPingPong);
  }

  // shortcuts
  else if(*p == 'q' || *p == 'w' || *p == 'e' || *p == 'r' || *p == 't') {
      // save position when stopped
      currentPositionSaved = false;

      // stop looping
      resetPingPong();

      // determine shortcut position
      int32_t dest = 0;
      if (*p == 'q') {
        dest = (ddFlip) ? minPos : maxPos;
      }
      else if (*p == 'w') {
        dest = (ddFlip) ? maxPos/2-maxPos/4 : maxPos/2+maxPos/4;
      }
      else if (*p == 'e') {
        dest = maxPos/2;
      }
      else if (*p == 'r') {
        dest = (ddFlip) ? maxPos/2+maxPos/4 : maxPos/2-maxPos/4;
      }
      else if (*p == 't') {
        dest = (ddFlip) ? maxPos : minPos;
      }

      // go to target
      // when stopped
      if(stepper.speed() == 0) {
        runTo(SHORTCUT_SPEED,SHORTCUT_ACC,dest);
      }
      // when in motion
      else if(!ddIsMovingToShortcut) {
        // prevent from changing shortcut target when running
        ddIsMovingToShortcut = true;

        // go to target with SHORTCUT_SPEED
        stepper.moveTo(dest);
        changeSpeedImmediately(SHORTCUT_SPEED);
      }

      // print debug message
      if(DEBUG_OUTPUT) ble_printf("\n Move to: %d\n", dest/currentMicrostepping);
    } 

    // parse speed and accerlation
    else {
      // incoming data
      char *str;

      // choose delimiters
      const char delimiters[] = "#,";

      // take incoming data and slice it into 2 chunks depending on the delimiters
      int32_t  inputParams[2];
      uint32_t counter = 0;
      uint32_t arraysize = sizeof(inputParams) / sizeof(int32_t);

      // reset input parameter array
      for (uint32_t i = 0; i < arraysize; i++) {
        inputParams[i] = NULL;
      }

      // fill the input parameter arraay with the sliced data
      while ((str = strtok_r(p, delimiters , &p)) != NULL) {
        inputParams[counter++] = strtol(str, NULL, NULL);
      }
  
      // print the values of the found and sliced input parameters
      // for (int i = 0; i < arraysize; ble_printf("\n%d ",inputParams[i++]));

      // assign names the input parameters
      int spd = inputParams[0]; //atoi(p);
      int acc = inputParams[1]; //atoi(...);

      // change speed and acceleration
      if (abs(spd) > 0 && abs(spd) <= MAX_RUN_SPEED) {
          // set new speed
          ddSpeed = spd;

          // update speed to restore direction
          if((!ddFlip && ddTarget == minPos) || (ddFlip && ddTarget == maxPos)) ddSpeed *= -1;

          // set new acceleration (determine automatically if none was specified or could be read)
          ddAcc = (acc == 0) ? getAccelerationFromSpeed(ddSpeed) : acc;

          // change speed (when not going to shortcut)
          if(!ddIsMovingToShortcut) {

             // prevent speed change to close to a limit switch (else there is not enough time for proper deceleration and the limit switch will be hit)
             if(acc != 0) {
               float threshold = (abs(ddSpeed) - ddAcc) * 2.5;
               if(threshold < END_THRESHOLD) { threshold = END_THRESHOLD; }
               if(abs(stepper.distanceToGo()) >= threshold*currentMicrostepping) { setSpeedAndAcc(ddSpeed,ddAcc); }   
             }
             // change speed immediately
             else {
              if(ddImmediateSpeedChange) changeSpeedImmediately(ddSpeed); 
             }
         }

         // print debug messages
         if(DEBUG_OUTPUT) {
            ble_printf("\n Speed: %d", ddSpeed);
            ble_printf(", Acceleration: %d\n", ddAcc);
          }
         
      }
  }
}

void settingsParser() {

  // get command
  char *p = inData; 

  if (*p == '+')
  {
    // exit via exitCondition
    settingsExit = true;
  }

  // save calibration
  // (use uppercase letter for system functions) 
  else if (*p == 'S') {
    // save calibration to EEPROM
    saveCalibrationToEEPROM();
  }
  
  // load calibration
  // (use uppercase letter for system functions)
  else if (*p == 'L')  {

    // set as uncalibrated
    calibrated = false;

    // load calibration from EEPROM
    loadCalibrationFromEEPROM();

    // move slider and save position
    if(stepper.currentPosition() != maxPos/2) {

       // save position when stopping
      currentPositionSaved = false;

      // move to center of the rail
      slideTo(MAX_CALIB_SPEED,getAccelerationFromSpeed(MAX_CALIB_SPEED),maxPos/2);
    }
  }
  
  // erase calibration and EEPROM
  // (use uppercase letter for system functions)
  else if (*p == 'E') {
    // erase EEPROM
    eraseEEPROM();

    // print debug message
    if(DEBUG_OUTPUT) ble_printf("\n--- EEPROM erased --- \n");
  }
  
  // print calibration
  // (use uppercase letter for system functions)
  else if (*p == 'P') {
    // print calibration information
    printCalibration();
  }

  // Overwrite initialization
  // (use uppercase letter for system functions)
  else if (*p == 'I') {
    // initialize
    //initialize();

    initialized = false;
    if(DEBUG_OUTPUT) ble_printf("\n--- Initialization --- \n");
  }

  // debug messages
  else if (*p == 'm' || *p == 'M') {
    switchDebug();
  }

  // parse speed and accerlation
  else {
    if(!initialized) {
      // incoming data
      char *str;

      // choose delimiters
      const char delimiters[] = "#,";

      // take incoming data and slice it into 2 chunks depending on the delimiters
      int32_t  inputParams[2];
      uint32_t counter = 0;
      uint32_t arraysize = sizeof(inputParams) / sizeof(int32_t);

      // reset input parameter array
      for (uint32_t i = 0; i < arraysize; i++) {
        inputParams[i] = NULL;
      }

      // fill the input parameter arraay with the sliced data
      while ((str = strtok_r(p, delimiters , &p)) != NULL) {
        inputParams[counter++] = strtol(str, NULL, NULL);
      }
  
      // print the values of the found and sliced input parameters
      // for (int i = 0; i < arraysize; ble_printf("\n%d ",inputParams[i++]));

      // assign names the input parameters
      int microstepping = inputParams[0]; //atoi(p);
      int camera = inputParams[1]; //atoi(...);

      if(DEBUG_OUTPUT) ble_printf("\n--- Overwriting Initalization ---\n");

      // set parameters
      currentMicrostepping = microstepping;
      currentCamera = camera;
  
      // save and print initialization
      saveInitializationToEEPROM();
      printInitialization();
      initialized = true;
    }
  }
}
