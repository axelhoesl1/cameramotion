
// Loop of the looping mode
void pingPongLoop() {
  if (initialized && calibrated && ddPingPong) {
      // change direction on motor side
      if (maxPoint) {
        if (stepper.distanceToGo() == 0) {
          if(!ddImmediateSpeedChange || (ddImmediateSpeedChange && stepper.currentPosition() == maxPos)) {

            // manage looping flags
            maxPoint = false;
            minPoint = true;

            // set speed and accerleration
            int spd = ddSpeed; 
            int acc = ddAcc; 

            if(ddVertical && ddVerticalFlip) {
                spd =  MAX_RUN_SPEED;
                acc =  MAX_RUN_SPEED;
            }

            // move the slider to the other end
            slideTo(spd,acc,minPos);
          }
        }
      }
      // change direction on end side
      else if (minPoint) {
        if (stepper.distanceToGo() == 0) {
          if(!ddImmediateSpeedChange || (ddImmediateSpeedChange && stepper.currentPosition() == minPos)) {
            
            // manage looping flags
            maxPoint = true;
            minPoint = false;

             // set speed and accerleration
            int spd = ddSpeed;
            int acc = ddAcc;
  
            if(ddVertical && !ddVerticalFlip) {
              spd = MAX_RUN_SPEED;
              acc = MAX_RUN_SPEED;
            }

            // move the slider to the other end
            slideTo(spd,acc,maxPos);
        }
      }
    }
  }
}

// Shoot-Move-Shoot Loop
void shootMoveShootLoop() {
      
  if(doSMS) {
      // move to start position
      if (smsStart) {

        // move to first waypoint
        if (stepper.distanceToGo() == 0) {
          smsStart = false;
          slideTo(smsSpeed,smsAcc,smsStartPos);
          
          smsRun = true;

          if(DEBUG_OUTPUT) ble_printf("\n Shoot Move Shoot start position: %d\n", smsStartPos/currentMicrostepping);
        }
      }

      // move to next position until end
      else if (smsRun && stepper.distanceToGo() == 0) {

        // move slider until end position is reached
        if (stepper.currentPosition() != smsEndPos) {

          // trigger a shutter release
          if ((unsigned long)(millis()-smsPreviousMillis) >= smsPictureIntervall-smsDeshakeDelay+smsShutterDelay) {
            smsPreviousMillis = millis();
            delay(smsDeshakeDelay);
            releaseShutter();
            delay(smsShutterDelay);
            currentPositionSaved = false;

            // move to next point
            int smsNextPoint = (smsStartPos < smsEndPos) ? stepper.currentPosition()+smsIntervall : stepper.currentPosition()-smsIntervall;
            if((smsStartPos < smsEndPos && smsNextPoint > smsEndPos) || (smsStartPos > smsEndPos && smsNextPoint < smsEndPos)) smsNextPoint = smsEndPos;
            
            slideTo(smsSpeed,smsAcc,smsNextPoint);
          }
        }
        // on end position
        else if (smsRun && stepper.currentPosition() == smsEndPos) {
          // print end message
          if(DEBUG_OUTPUT) {
            ble_printf("\n Shoot Move Shoot end position: %d\n", stepper.currentPosition()/currentMicrostepping);
            ble_printf("Pictures taken: %d\n", smsPicturesTaken);
            ble_printf("--- Exit Shoot-Move-Shoot ---\n");
          }

          // stop and reset 
          smsRun = false;
          doSMS = false;
          hardStop();

          smsPicturesTaken = 0;
          smsLastRelease = 0;
          state = IDLE_STATE;
        }
      }
    }
}
