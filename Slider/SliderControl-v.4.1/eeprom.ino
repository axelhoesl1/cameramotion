
// Load saved initialization data from EEPROM
// Data consists of: currentMicrostepping, currentCamera
void loadInitializationFromEEPROM() {

  // Select EEPROM
  I2CEEPROM myEEPROM;  

  // Read int from currentMicrostepping field
  int32_t currentMicrosteppingRead = myEEPROM.read_int32_t(EEPROM_MICROSTEPPING*EEPROM_FIELD_LENGTH);

  // Read int from currentCamera field
  int32_t currentCameraRead        = myEEPROM.read_int32_t(EEPROM_CAMERA*EEPROM_FIELD_LENGTH);

  // Check for errors in reading currentMicrostepping and reading currentCamera
  if(!(currentMicrosteppingRead == 2 || currentMicrosteppingRead == 4 || currentMicrosteppingRead == 8 || currentMicrosteppingRead == 16 || 
       currentMicrosteppingRead == 32 || currentMicrosteppingRead == 64 || currentMicrosteppingRead == 128 || currentMicrosteppingRead == 256) ||
       (currentCameraRead < 0 || currentCameraRead >= NUM_CAMS) ) {
      initialized = false;
      printInitializationPrompt();
  }
  // If no errors are found set the read values
  else {
    // If currentMicrosteppingRead == 0 -> 1 because of later multiplications
    currentMicrostepping = (currentMicrosteppingRead == 0) ? 1 : currentMicrosteppingRead;

    currentCamera = currentCameraRead;
    initialized = true;
    printInitialization();
  }
}

// Load saved initialization data from EEPROM
// Data consists of: maximum and minimum position of the rail (in motor steps), position of the slider (in motor steps)
void loadCalibrationFromEEPROM() {
   // Select EEPROM
  I2CEEPROM myEEPROM;

  // Read int from maxPos field
  int32_t maxPosRead = myEEPROM.read_int32_t(EEPROM_MAXPOS*EEPROM_FIELD_LENGTH);

  // Read int from minPos field
  int32_t minPosRead = myEEPROM.read_int32_t(EEPROM_MINPOS*EEPROM_FIELD_LENGTH);

  // Read int from pos field
  long posRead = myEEPROM.read_int32_t(EEPROM_POS*EEPROM_FIELD_LENGTH);

  // Check for errors
  if((minPosRead == 0 && maxPosRead == 0) || posRead < 0 || posRead < minPosRead || posRead > maxPosRead) {
      calibrated = false;
      printCalibrationPrompt();
  }
  // If no errors are found set the read values
  else {
    maxPos = maxPosRead;
    minPos = minPosRead;
    stepper.setCurrentPosition(posRead);
    calibrated = true;
    printCalibration();
  } 
}

// Save initialization data to EEPROM
// Data consists of: currentMicrostepping, currentCamera
void saveInitializationToEEPROM() { 

  // Select EEPROM
  I2CEEPROM myEEPROM;

  // Write int to currentMicrostepping field
  myEEPROM.write_int32_t(EEPROM_MICROSTEPPING*EEPROM_FIELD_LENGTH, currentMicrostepping);

  // Write int to currentCamera field
  myEEPROM.write_int32_t(EEPROM_CAMERA*EEPROM_FIELD_LENGTH, currentCamera); 

  // Print debug message
  if(DEBUG_OUTPUT) ble_printf("\n--- Initialization saved to EEPROM --- \n");
}

// Save calibration data to EEPROM
// Data consists of: maximum and minimum position of the rail (in motor steps), position of the slider (in motor steps)
void saveCalibrationToEEPROM() {

  // Select EEPROM
  I2CEEPROM myEEPROM;

  // Write int to maxPos field
  myEEPROM.write_int32_t(EEPROM_MAXPOS*EEPROM_FIELD_LENGTH, maxPos);

  // Write int to minPos field
  myEEPROM.write_int32_t(EEPROM_MINPOS*EEPROM_FIELD_LENGTH, minPos);

  // Write long to pos field
  long pos = stepper.currentPosition();
  myEEPROM.write_int32_t(EEPROM_POS*EEPROM_FIELD_LENGTH, pos);

  // Print debug message
  if(DEBUG_OUTPUT) ble_printf("\n--- Calibration saved to EEPROM --- \n");
}

// Save position data to EEPROM
// Data consists of: position of the slider (in motor steps)
void saveCurrentPositionToEEPROM() {

  // Select EEPROM
  I2CEEPROM myEEPROM;

  // Write long to pos field
  long pos = stepper.currentPosition();
  myEEPROM.write_int32_t(EEPROM_POS*EEPROM_FIELD_LENGTH, pos);

  // Print debug message 
  if(DEBUG_OUTPUT) ble_printf("\n--- Current position saved to EEPROM --- \n (pos: %d)\n", stepper.currentPosition()/currentMicrostepping);

  // set flag
  currentPositionSaved = true;
}
