// Entry conditions of finite state machine 
// These entry conditions should execute an entry function only once.
void entryCondition()
{

   // Entering Direct-Drive
  if (initialized && sercom[0] == 1) {
    state = DIRECT_DRIVE;
    directDrive();
  }

  // Entering Programmed-Drive
  else if (initialized && sercom[0] == 2) {
    if (checkPoints()) {
      state = PROGRAMMED_DRIVE;
      select_runmode = &AccelStepper::run;
    }
  }

  // Entering Shoot-Move-Shoot-Drive
  else if (initialized && sercom[0] == 3) {
    state = SHOOT_MOVE_SHOOT;
    setSpeedAndAcc(smsSpeed,smsAcc);
    shootMoveShoot();
  }
 
  // Entering settings 
  else if (sercom[0] == 9){
    state = SETTINGS;
    settings();
  }
  
}

// Exit conditions of finite state machine
// These exit conditions should execute an exit function only once.
void exitCondition()
{

  // Reset interrupts
  if(interrupted) interrupted = false;

  // Go to idle state
  if (state == IDLE_STATE) {
    hardStop();
    //Do Nothing aka idle state!
  }

  // Exit calibration
  else if(state == DIRECT_DRIVE && !calibrated){
      if(!calibrated && maxPos != 0) {
        stepper.setCurrentPosition(maxPos/2);
        calibrated = true;
        currentPositionSaved = false;
        printCalibration();
        saveCalibrationToEEPROM();
        switchesProtectionMode(); 
        ble_printf("Calibration finished\n");
    }
  }

  // Exit Direct-Drive 
  else if(state == DIRECT_DRIVE) {
    // when stopped after moving to a shortcut
    if(!ddExit && ddIsMovingToShortcut && stepper.speed() == 0) ddIsMovingToShortcut = false;

    // when exiting from direct drive mode
    if(ddExit){
      ddExit = false;
      ddIsMovingToShortcut = false;
      hardStop();
      currentPositionSaved = false;
      resetPingPong();
      ble_printf("\n--- Exit DirectDrive ---\n");
      select_parser = &standardCommandParser;
      state = IDLE_STATE;
    }
  }

  // Exit Programmed-Drive
  else if(state == PROGRAMMED_DRIVE){
    programmedDrive();
  }

  // Save settings when overwriting
  else if(state == SETTINGS) {
    if(settingsExit) {
      settingsExit = false;
      saveInitializationToEEPROM();
      ble_printf("\n--- Exit Settings ---\n");
      select_parser = &standardCommandParser;
      state = IDLE_STATE;
    }
  }

}
