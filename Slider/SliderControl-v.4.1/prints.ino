// Print initialization prompt
void printInitializationPrompt() {
  ble_printf("\nInitialize slider (9#,I#,MICROSTEPPING,CAMERA#) \n");
}

// Print calibration prompt
 void printCalibrationPrompt() {
  if(state != DIRECT_DRIVE) {
    ble_printf("\nEnter calibration mode (1#) and calibrate (c#): \n"); 
  }
  else {
    ble_printf("\nCalibration necessary (c#) \n");
  }
 }

 void printStartPrompt() {
  ble_printf("--------------------------- \n");
  ble_printf("Camera Slider v4.1 alpha \n");
  ble_printf("--------------------------- \n");
 }

/*
Helper to print all parameters of the motor at once.
*/
void printStepperParameter()
{
  if(calibrated) {
    ble_printf("\r\n[+] Slider parameters:\n");
    ble_printf("      Minimum: %d", minPos/currentMicrostepping);
    ble_printf(", Maximum: %d\n", maxPos/currentMicrostepping);
    ble_printf("      Position: %d", stepper.currentPosition()/currentMicrostepping);
    ble_printf(", Target: %d\n", stepper.targetPosition()/currentMicrostepping);
    ble_printf("\n      Speed: ");
    ble_print_float((stepper.speed()/currentMicrostepping), FLOAT_PRECISION);
    ble_printf("\n      Microstepping: %d\n", currentMicrostepping);
    ble_printf("      Current Camera: %s\n", enum_cams[currentCamera]);
  }
}

void printInitialization() {
  ble_printf("\n--- Current Initialization --- \n");
  ble_printf("     Microstepping: %d, ", currentMicrostepping);
  ble_printf("     Camera: %s \n", enum_cams[currentCamera]);
}

void printCalibration() {
  ble_printf("\n--- Current Calibration ---\n");
  ble_printf("     minPos: %d", minPos/currentMicrostepping);
  ble_printf("     maxPos: %d", maxPos/currentMicrostepping);
  ble_printf("     currentPosition: %d\n", stepper.currentPosition()/currentMicrostepping);
}

void printWaypointParameter()
{
  #ifdef DEBUG_OUTPUT == 1
  Serial.println("\r\n[+] Next waypoint: ");
  Serial.print("     Position: ");
  Serial.println(*nextPoint);
  Serial.print("     Speed: ");
  Serial.println(*nextSpeed);
  Serial.print("     Acceleration: ");
  Serial.println(*nextAcc);
  #endif
          
  #ifdef DEBUG_OUTPUT == 0
  ble_printf("\r\n[+] Next waypoint:\n");
  ble_printf("      Position: %d\n", *nextPoint);
  ble_printf("      Speed: %d\n",*nextSpeed);
  ble_printf("      Acceleration: %d\n",*nextAcc);
  #endif
}

void printCurrentPosition() {
  ble_printf("\n currentPosition: %d, ", stepper.currentPosition()/currentMicrostepping);

  I2CEEPROM myEEPROM;
  int pos = myEEPROM.read_int32_t(EEPROM_POS*EEPROM_FIELD_LENGTH);
  ble_printf("currentPosition in EEPROM: %d \n", pos/currentMicrostepping);
}

void eraseEEPROM() {
  maxPos = 0;
  minPos = 0;
  stepper.setCurrentPosition(0);
  
  saveCalibrationToEEPROM();
}

void printDDMode() {
  ble_printf("\n DDmode \n");  
}
