$fn=300;

//Zugabe

z = 0.3;
prot = 0.15;

//Abmessungen Schalter

dicke = 6.80+z;

laenge_seg_oben = 19.35+z;
laenge_seg_mitte = 18.15+z;
laenge_seg_unten = 12.70+z;
laenge_seg_boden = laenge_seg_unten;

hoehe_seg_oben = 3.50+z;
hoehe_seg_mitte = 2.30+z;
hoehe_seg_unten = 6.30+z;
hoehe_seg_boden = 4.00+z;

radius_bolzen = 1.45+z/2;
laenge_bolzen = 9.75;

radius_bohrung = 1.60+z/2;

// Abmessungen Halterung
 
wanddicke = 1.50;

hoehe_halterung = hoehe_seg_oben+hoehe_seg_mitte+hoehe_seg_unten+hoehe_seg_boden;
laenge_halterung = laenge_seg_oben+wanddicke*2;
dicke_halterung = laenge_bolzen+wanddicke*2;

// Hauptkörper, umfasst den Schalter

module schalter(){

	// Laengeres Teilsegment oben

	translate([0, 0, hoehe_seg_unten+hoehe_seg_boden+hoehe_seg_mitte]){
		cube ([laenge_seg_oben, dicke, hoehe_seg_oben]);
	}

	// Mittelsegment

	translate([laenge_seg_oben-laenge_seg_mitte, 0, hoehe_seg_unten+hoehe_seg_boden]){
		cube ([laenge_seg_mitte, dicke, hoehe_seg_mitte+prot]);
	}

	// Kürzeres Teilsegment unten

	translate([laenge_seg_oben-laenge_seg_mitte, 0, hoehe_seg_boden]){
		cube ([laenge_seg_unten, dicke, hoehe_seg_unten+prot]);
	}

	// Puffersegment unten für Kabelausgang

	translate([laenge_seg_oben-laenge_seg_mitte, 0, -prot]){
		cube([laenge_seg_boden, dicke, hoehe_seg_boden+prot*2]);
	}

	// Bolzen

	translate([laenge_seg_oben-laenge_seg_mitte+radius_bolzen,laenge_bolzen-((laenge_bolzen-dicke)/2),hoehe_seg_boden+hoehe_seg_unten+radius_bolzen+0.5]){
		rotate(90,[1,0,0]){
			cylinder(r = radius_bolzen, h= laenge_bolzen);
		}	
	}
	translate([laenge_seg_oben-laenge_seg_mitte, -((laenge_bolzen-dicke)/2), hoehe_seg_boden+hoehe_seg_unten+radius_bolzen+0.5-prot]){
		cube([radius_bolzen*2, laenge_bolzen, hoehe_seg_oben+radius_bolzen+prot]);
	}
}

// Achtung: Durch die Minkowski Summe vergroessert sich der Container, was bei der Laenge der Bohrung beachtet werden muss!

module container(){
		translate([-wanddicke, -(dicke_halterung/2)+dicke/2, -prot]){
			minkowski(){
				cube([laenge_halterung, dicke_halterung, hoehe_halterung]);
				cylinder(r = 1, h = 0.01);
			}
		}
		
}

//container();

module bohrung (){

	translate([laenge_seg_oben-radius_bohrung-1.3, (dicke_halterung/2)+dicke/2+2, hoehe_seg_boden+hoehe_seg_unten+hoehe_seg_mitte]){
		rotate(90,[1,0,0]){
			cylinder ( r = radius_bohrung, h = dicke_halterung+prot*2+3);
		}
	}
}

module body(){
	difference(){
		container();
		schalter();
	}
}

module main(){
	difference(){
		body();
		bohrung();
	}
}

main();
