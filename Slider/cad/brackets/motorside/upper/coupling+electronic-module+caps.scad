$fn=300;

// Zugabe

z = 0.30;
prot = 0.15;

// Abmessungen: C1 (Welle)
c1_r = 6.00/2.00;
c1_h = 4.00;

// Abmessungen: C2 (Puffer)
c2_r = 15.00/2.00 + z;
c2_h = 3.00;

// Abmessungen: C3 (Kugellager)
c3_r = 19.00/2.00 + z;
c3_h = 6.00;

// Abmessungen: Nema Bohrbild
nema_d    	= 47.14;
nema_r	  	= 3.00/2.00 + z;
nema_outline = 56.40;
nema_motorkern = 38.10;

// Abmessungen allgemein

modullaenge = 50.00;
breite_ausschnitt= 20.00;
hoehe_ausschnitt= modullaenge-12.00;
sockelgroesse = 4.00+z/2;

// Schutz
l_schutz = 3.00; 
b_schutz = 20.00;

// Abmessungen Elektronikmodul

breite_elektronikmodul = nema_outline;
hoehe_elektronikmodul = modullaenge;
dicke_elektronikmodul = 30.00;
wand_elektronikmodul = 2.00;

dicke_deckel = 1.00;
dicke_deckel_aussen = 2.00;

hoehe_sockel_elektronikmodul = dicke_elektronikmodul-dicke_deckel;
breite_sockel_elektronikmodul = 6.50;

r_eschrauben = (3.00+z)/2; // Diese Schrauben werden zur Befestigung beider Deckel verwendet
h_eschrauben = 10.00;

r_sockel_eschrauben = (5.30+z)/2;
h_sockel_eschrauben = 1.00;

// Befestigung der Platine

r_boardschrauben = (1.60+z)/2;
h_boardschrauben = 6.00;
breite_sockel_board = 5.00;
hoehe_sockel_board = 6.00;
randabstand_sockel_board_seitlich = 2.00;
randabstand_sockel_board_oben = 0.00;

// Kabeldurchgang

breite_kabeldurchgang = 24.00; 
hoehe_kabeldurchgang = 10.00;
radius_kabeldurchgang = (7.00+z)/2;

// Infrarot Modul

r_boardschrauben_ir = (1.60+z)/2;
h_boardschrauben_ir = 5.00;
breite_sockel_board_ir = 5.00;
hoehe_sockel_board_ir = 5.00;
randabstand_sockel_board_seitlich_ir = 10.00;
randabstand_sockel_board_hinten_ir = 27.00;
wand_ir_modul = 3.00;
dicke_ir_modul = 14.00;
sockel_ir_modul = 1.00;

dicke_deckel_ir = 1.00;
dicke_deckel_aussen_ir = 2.00;

r_kabeldurchgang_ir = (7.00+z)/2;
h_kabeldurchgang_ir = wand_ir_modul+2*prot;

// MODULE

//____________________KUPPLUNGSMODUL_______________________________________

// Wellenlauf

module wellenlauf(c1_r, c1_h, c2_r, c2_h, c3_r, c3_h){
    translate([0,0,-prot]) {
	cylinder(r=c3_r, h=c3_h+2*prot);
	}
	translate([0,0,c3_h-prot]){
		cylinder(r=c2_r, h=c2_h+2*prot);
	}
	translate([0,0,c3_h+c2_h]){
		cylinder(r=c1_r, h=c1_h+prot);
	}
}

module sicherheits_riegel() {
    translate([z/2,0,-prot]) {
       cube([l_schutz+z,b_schutz+z, 40]);
    }
}

// Hauptkörper

module body(nema_outline, modullaenge){
	translate([2,2,0]){
		hull(){
        translate([0,-dicke_ir_modul,0]){
            cylinder(r=2, h=modullaenge);}
        translate([55,-dicke_ir_modul,0]){
            cylinder(r=2, h=modullaenge);}
		translate([nema_outline-2+dicke_elektronikmodul,0,0]){
            cylinder(r=2, h=modullaenge);
		}
		translate([0,nema_outline-2,0]){
            cylinder(r=2, h=modullaenge);
		}
		translate([nema_outline-2+dicke_elektronikmodul,nema_outline-2,0]){
            cylinder(r=2, h=modullaenge);
		}
		
		}
	}
}

// Ausschnitte

module ausschnitte(){

	translate([nema_outline/2+1,nema_outline/2+1,0]){
		wellenlauf(c1_r, c1_h, c2_r, c2_h, c3_r, c3_h);
	}

// Kernzylinder

	translate([nema_outline/2+1, nema_outline/2+1,modullaenge-hoehe_ausschnitt]){
		cylinder(r=(nema_motorkern+z)/2, h=hoehe_ausschnitt+prot);
	}

// Seitlicher Ausschnitt

	translate([0,(nema_outline/2+1)-(breite_ausschnitt/2),modullaenge-hoehe_ausschnitt+prot]){
		hull(){
			cube([13,breite_ausschnitt, hoehe_ausschnitt+prot]);
			cylinder(r=2, h=hoehe_ausschnitt);
				translate([0,breite_ausschnitt,0]){
					cylinder(r=2, h= hoehe_ausschnitt);
				}
		}
	}

// Bohrlöcher
	translate([nema_outline/2+1, nema_outline/2+1, 0]){
		for ( i = [0 : 3]){
    		rotate( (i * 360 / 4)+45, [0, 0, 1]){
    			translate([0, (nema_d/2)*sqrt(2), 0]){
   					cylinder(r = nema_r, h=modullaenge+prot);
				}
			}	
		}
	}

// Sockel
	translate([(nema_outline)/2+1, (nema_outline)/2+1,-prot]){
		for ( i = [0 : 3]){
    		rotate( (i * 360 / 4)+45, [0, 0, 1]){
				translate([0, (nema_outline/2+2), 0]){
					rotate(45,[0,0,1]){
						hull(){
							translate([-sockelgroesse,-sockelgroesse,0]){
								cylinder(r=2, h=3*z+modullaenge/3);
							}
							translate([-sockelgroesse,sockelgroesse,0]){
								cylinder(r=2, h=3*z+modullaenge/3);
							}
							translate([sockelgroesse,-sockelgroesse,0]){
								cylinder(r=2, h=3*z+modullaenge/3);
							}
							translate([sockelgroesse,sockelgroesse,0]){
								cylinder(r=2, h=3*z+modullaenge/3);
							}
						}
					}
				}	
			}
		}
	}
    
    // Sicherheits Riegel
   translate([nema_outline-l_schutz-47,nema_outline/2-b_schutz/2+1-z/2,0]) {
       sicherheits_riegel();
    } 
    
    // IR Modul
    translate([-prot,-prot,2]){
		rotate(-90,[0,0,1]){
			cube([dicke_ir_modul,nema_outline-1,modullaenge-4]);
		}
	}
    
    //ir_ausschnitte();
    /*
    translate([nema_outline+prot,-dicke_ir_modul+prot,modullaenge/3]){
		rotate(90,[0,1,0]){
			cylinder(r=r_kabeldurchgang_ir, h=h_kabeldurchgang_ir+prot, center=true);
		}
	}
    */
    
		rotate(90,[0,1,0]){
            rotate(31,[-1,0,0]){
                translate([-modullaenge/2,-35,80]) {
                cylinder(r=r_kabeldurchgang_ir+1, h=80, center=true);
            }
		}
	}
    
    /*
    translate([dicke_ir_modul, nema_outline+0.5-prot,modullaenge/3]){
		rotate(90,[1,0,0]){
			cylinder(r=r_kabeldurchgang_ir, h=h_kabeldurchgang_ir+prot, center=true);
		}
	}
    */
}

module body_mit_ausschnitten(){
	difference(){
		body(nema_outline, modullaenge);
		ausschnitte();
	}
}

//_________________________ELEKTRONIK MODUL AUF RÜCKSEITE_________________________

// Serieller Stecker

module serial_plug(){
	translate([nema_outline-4+dicke_elektronikmodul,nema_outline+2+prot, modullaenge/2+15]){
		rotate(90,[0,1,0]){
			rotate(90,[1,0,0]){	
				difference(){
					translate([-3,-13,0]){
						cube([35,13,4]);
					}
					difference(){
						scale([1,1,2.0]){
							import ("DB9Mount.stl");
						}
						translate([-10,2,0]){
							cube([50,40,3]);
						}
					}
				}
			}
		}
	}
}


//Test für den seriellen Anschluss

module serial_plug_test(){
	difference(){
		translate([-3,-15,0]){
			cube([35,20,2]);
		}
		difference(){
			translate([-3,-13,0]){
				cube([35,13,4]);
			}
			difference(){
				scale([1,1,2.0]){
					import ("DB9Mount.stl");
				}
				translate([-10,2,0]){
					cube([50,40,3]);
				}
			}
		}
	}
}


//serial_plug();

module alles_ohne_elektronik(){
	translate([nema_outline+dicke_elektronikmodul-dicke_deckel-1.00,nema_outline-4.00,wand_elektronikmodul]){
cube([3.00,3.00,modullaenge-wand_elektronikmodul*2-breite_sockel_elektronikmodul]);
}
	difference(){
		body_mit_ausschnitten();
		translate([nema_outline+wand_elektronikmodul+prot+2, wand_elektronikmodul+1, wand_elektronikmodul]){
			cube([dicke_elektronikmodul-wand_elektronikmodul, breite_elektronikmodul+prot-wand_elektronikmodul*2, hoehe_elektronikmodul-wand_elektronikmodul*2]);
		}
	}	
}

module schraubsockel(){

// Befestigung des Deckels

translate([nema_outline+dicke_elektronikmodul+2-dicke_deckel-hoehe_sockel_elektronikmodul-prot,wand_elektronikmodul+1,wand_elektronikmodul]){
	difference(){
		cube([hoehe_sockel_elektronikmodul+prot, breite_sockel_elektronikmodul, breite_sockel_elektronikmodul]);
		translate([hoehe_sockel_elektronikmodul-h_eschrauben+prot*2,breite_sockel_elektronikmodul/2,breite_sockel_elektronikmodul/2]){
			rotate(90,[0,1,0]){
				cylinder( r = r_eschrauben,h = h_eschrauben);
			}
		}
	}
}

translate([nema_outline+dicke_elektronikmodul+2-dicke_deckel-hoehe_sockel_elektronikmodul-prot,nema_outline-breite_sockel_elektronikmodul-1,modullaenge-wand_elektronikmodul-breite_sockel_elektronikmodul]){
	difference(){
		cube([hoehe_sockel_elektronikmodul+prot, breite_sockel_elektronikmodul, breite_sockel_elektronikmodul]);
		translate([hoehe_sockel_elektronikmodul-h_eschrauben+prot*2,breite_sockel_elektronikmodul/2,breite_sockel_elektronikmodul/2]){
			rotate(90,[0,1,0]){
				cylinder( r = r_eschrauben,h = h_eschrauben);
			}
		}
	}
}

// Befestigung des Boards

translate([nema_outline+2-prot,wand_elektronikmodul+1+randabstand_sockel_board_seitlich,modullaenge-wand_elektronikmodul-breite_sockel_board-randabstand_sockel_board_oben+prot]){
	difference(){
		cube([hoehe_sockel_board, breite_sockel_board, breite_sockel_board]);
		translate([prot,breite_sockel_board/2,breite_sockel_board/2]){
			rotate(90,[0,1,0]){
				cylinder( r = r_boardschrauben,h = h_boardschrauben);
			}
		}
	}
}

translate([nema_outline+2-prot,wand_elektronikmodul+1+randabstand_sockel_board_seitlich+22.80,modullaenge-wand_elektronikmodul-breite_sockel_board-randabstand_sockel_board_oben+prot]){
	difference(){
		cube([hoehe_sockel_board, breite_sockel_board, breite_sockel_board]);
		translate([prot,breite_sockel_board/2,breite_sockel_board/2]){
			rotate(90,[0,1,0]){
				cylinder( r = r_boardschrauben,h = h_boardschrauben);
			}
		}
	}
}

translate([nema_outline+2-prot,wand_elektronikmodul+1+randabstand_sockel_board_seitlich,modullaenge-wand_elektronikmodul-breite_sockel_board-randabstand_sockel_board_oben-35.00]){
	difference(){
		cube([hoehe_sockel_board, breite_sockel_board, breite_sockel_board]);
		translate([prot,breite_sockel_board/2,breite_sockel_board/2]){
			rotate(90,[0,1,0]){
				cylinder( r = r_boardschrauben,h = h_boardschrauben);
			}
		}
	}
}

translate([nema_outline+2-prot,wand_elektronikmodul+1+randabstand_sockel_board_seitlich+22.80,modullaenge-wand_elektronikmodul-breite_sockel_board-randabstand_sockel_board_oben-35.00]){
	difference(){
		cube([hoehe_sockel_board, breite_sockel_board, breite_sockel_board]);
		translate([prot,breite_sockel_board/2,breite_sockel_board/2]){
			rotate(90,[0,1,0]){
				cylinder( r = r_boardschrauben,h = h_boardschrauben);
			}
		}
	}
}


}

module kabel(){
	translate([nema_outline+8+prot+dicke_elektronikmodul/3+1,(nema_outline+2)*2/3-breite_kabeldurchgang+3, -prot]){
		cube([hoehe_kabeldurchgang, breite_kabeldurchgang, wand_elektronikmodul+2*prot]);
	}

    /*
	translate([nema_outline+3.5+dicke_elektronikmodul-1.7,wand_elektronikmodul+1+prot, modullaenge/3]){
		rotate(90,[1,0,0]){
			cylinder( r = radius_kabeldurchgang, h= wand_elektronikmodul+1+2*prot);
		}
	}
    */
}

module alles_mit_elektronik(){
	difference(){
		alles_ohne_elektronik();
		serial_plug();
	}
	schraubsockel();
}

module bauteil_ohne_deckel(){
	difference(){
		alles_mit_elektronik();
		kabel();
	}
}

// Deckelverschluss für Elektronik Kompartement

module deckel(){
	cube([modullaenge, nema_outline, dicke_deckel_aussen]);
	translate([wand_elektronikmodul+0.50*z, wand_elektronikmodul+0.50*z, dicke_deckel_aussen-prot]){
		cube([modullaenge-wand_elektronikmodul*2-z, nema_outline-wand_elektronikmodul*2-z, dicke_deckel+prot]);
	}
}

// Bohrlöcher des Deckels

module deckel_bohr(){
	translate([wand_elektronikmodul+breite_sockel_elektronikmodul/2,wand_elektronikmodul+breite_sockel_elektronikmodul/2,0]){
		cylinder( r = r_eschrauben,h = dicke_deckel+dicke_deckel_aussen+2*prot);
		cylinder( r1 = r_sockel_eschrauben, r2=r_eschrauben, h = h_sockel_eschrauben);
	}
	translate([modullaenge-wand_elektronikmodul-breite_sockel_elektronikmodul/2,nema_outline-wand_elektronikmodul-breite_sockel_elektronikmodul/2,0]){
		cylinder( r = r_eschrauben,h = dicke_deckel+dicke_deckel_aussen+2*prot);
		cylinder( r1 = r_sockel_eschrauben, r2=r_eschrauben, h = h_sockel_eschrauben);
	}
}


module deckel_gesamt(){
	difference(){
		deckel();
		deckel_bohr();
	}
}

// Verschiebung des Deckels für Testzwecke
// Für Test mit Elektronikmodul folgendes als x-Translation: nema_outline+dicke_elektronikmodul+dicke_deckel+2

module deckel_test(shift){
	translate([nema_outline+dicke_elektronikmodul+dicke_deckel+2+shift,1,0]){
		rotate(-90,[0,1,0]){
			deckel_gesamt();
		}
	}
}

//__________________INFRAROT MODUL____________________________

// Körper des IR Modul

module ir_body(){
	translate([dicke_ir_modul,0,0]){
		rotate(-90,[0,1,0]){
			translate([2,2,0]){
				hull(){
					cylinder(r=2, h=dicke_ir_modul);
					translate([modullaenge-4,0,0]){
						cylinder(r=2, h=dicke_ir_modul);
					}
					translate([0,nema_outline-2,0]){
						cylinder(r=2, h=dicke_ir_modul);
					}
					translate([modullaenge-4,nema_outline-2,0]){
						cylinder(r=2, h=dicke_ir_modul);
					}
				}
			}
		}
	}
}

//Schraubsockel für das IR Modul

module schraubsockel_ir(){

// Deckelsockel x2

	translate([sockel_ir_modul-prot,wand_ir_modul,wand_ir_modul]){
		difference(){
			cube([dicke_ir_modul-sockel_ir_modul-dicke_deckel_ir+2*prot, breite_sockel_elektronikmodul, breite_sockel_elektronikmodul]);
			translate([dicke_ir_modul-h_eschrauben+prot,breite_sockel_elektronikmodul/2,breite_sockel_elektronikmodul/2]){
				rotate(90,[0,1,0]){
					cylinder( r = r_eschrauben,h = h_eschrauben);
				}
			}
		}
	}

	translate([sockel_ir_modul-prot,nema_outline-wand_ir_modul-breite_sockel_elektronikmodul,modullaenge-2-breite_sockel_elektronikmodul]){
		difference(){
			cube([dicke_ir_modul-sockel_ir_modul-dicke_deckel_ir+2*prot, breite_sockel_elektronikmodul, breite_sockel_elektronikmodul]);
			translate([dicke_ir_modul-h_eschrauben+prot,breite_sockel_elektronikmodul/2,breite_sockel_elektronikmodul/2]){
				rotate(90,[0,1,0]){
					cylinder( r = r_eschrauben,h = h_eschrauben);
				}
			}
		}
	}

// Boardsockel x2



	translate([0,nema_outline+2-randabstand_sockel_board_hinten_ir-17.5,wand_ir_modul+randabstand_sockel_board_seitlich_ir]){
		rotate(90,[1,0,0]){
			rotate(90,[0,1,0]){
				difference(){
					cube([breite_sockel_board_ir, breite_sockel_board_ir, hoehe_sockel_board_ir]);
					translate([breite_sockel_board_ir/2,breite_sockel_board_ir/2,prot]){
						cylinder( r = r_boardschrauben_ir,h = h_boardschrauben_ir);
					}
				}
                
       translate([17.5,0,0]) {         
              
                difference(){
					cube([breite_sockel_board_ir, breite_sockel_board_ir, hoehe_sockel_board_ir]);
					translate([breite_sockel_board_ir/2,breite_sockel_board_ir/2,prot]){
						cylinder( r = r_boardschrauben_ir,h = h_boardschrauben_ir);
					}
				}
            }

//Translation für IR Board spezifisch
                translate([0,27.7,0]){
					difference(){
						cube([breite_sockel_board_ir, breite_sockel_board_ir, hoehe_sockel_board_ir]);
						translate([breite_sockel_board_ir/2,breite_sockel_board_ir/2,prot]){
							cylinder( r = r_boardschrauben_ir,h = h_boardschrauben_ir);
				
						}
					}
				}

				translate([17.5,27.7,0]){
					difference(){
						cube([breite_sockel_board_ir, breite_sockel_board_ir, hoehe_sockel_board_ir]);
						translate([breite_sockel_board_ir/2,breite_sockel_board_ir/2,prot]){
							cylinder( r = r_boardschrauben_ir,h = h_boardschrauben_ir);
				
						}
					}
				}
			}
		}
	}
}

module ir_ausschnitte(){
	translate([dicke_ir_modul, nema_outline+0.5-prot,modullaenge/3]){
		rotate(90,[1,0,0]){
			cylinder(r=r_kabeldurchgang_ir, h=h_kabeldurchgang_ir+prot, center=true);
		}
	}
}

module ir_module(){
    /*
	difference(){
		difference(){
			ir_body();
			translate([sockel_ir_modul,-prot,wand_ir_modul]){
				cube([dicke_ir_modul-prot, nema_outline+2-wand_ir_modul+prot, modullaenge-wand_ir_modul*2]);
			}
		}
		ir_ausschnitte();
	}
    */
	schraubsockel_ir();
}

module deckel_ir_modul(){

	translate([dicke_deckel_aussen_ir,0,0]){
		rotate(-90,[0,1,0]){
			translate([2,2,0]){
				hull(){
					cylinder(r=2, h=dicke_deckel_aussen_ir);
					translate([modullaenge-4,0,0]){
						cylinder(r=2, h=dicke_deckel_aussen_ir);
					}
					translate([0,nema_outline-2,0]){
						cylinder(r=2, h=dicke_deckel_aussen_ir);
					}
					translate([modullaenge-4,nema_outline-2,0]){
						cylinder(r=2, h=dicke_deckel_aussen_ir);
					}
				}
			}
		}
	}
	translate([-dicke_deckel_ir+prot,wand_ir_modul,wand_ir_modul]){
		cube([dicke_deckel_ir, nema_outline+2-wand_ir_modul*2, modullaenge-wand_ir_modul*2]);
	}
}

module deckel_ir_modul_mit_bohrungen(){
	difference(){
		deckel_ir_modul();
		translate([0,wand_ir_modul,wand_ir_modul]){
			translate([-dicke_deckel_ir-prot,breite_sockel_elektronikmodul/2,breite_sockel_elektronikmodul/2]){
				rotate(90,[0,1,0]){
					cylinder( r = r_eschrauben,h = dicke_deckel_ir+dicke_deckel_aussen_ir+2*prot);
					translate([0,0,dicke_deckel_aussen_ir+h_sockel_eschrauben/2]){
						cylinder( r1 = r_eschrauben, r2=r_sockel_eschrauben, h = h_sockel_eschrauben);	
					}
				}
			}		
		}

		translate([0,nema_outline-wand_ir_modul-breite_sockel_elektronikmodul,modullaenge-2-breite_sockel_elektronikmodul]){
			translate([-dicke_deckel_ir-prot,breite_sockel_elektronikmodul/2,breite_sockel_elektronikmodul/2]){
				rotate(90,[0,1,0]){
					cylinder( r = r_eschrauben,h = dicke_deckel_ir+dicke_deckel_aussen_ir+2*prot);
					translate([0,0,dicke_deckel_aussen_ir+h_sockel_eschrauben/2]){
						cylinder( r1 = r_eschrauben, r2=r_sockel_eschrauben, h = h_sockel_eschrauben);	
					}
				}
			}
		}
	}
}

// Testzwecke Passung Deckel IR Modul

module deckel_test_ir(shift2){
	translate([0,prot,0]){
		rotate(-90,[0,0,1]){
			translate([dicke_ir_modul+shift2,0,0]){
				deckel_ir_modul_mit_bohrungen();
			}
		}
	}
}

module bauteil_mit_allen_modulen(){
	bauteil_ohne_deckel();
	translate([0,prot,0]){
		rotate(-90,[0,0,1]){
			ir_module();
		}
	}
}

//Erstelle eine Testschablone für den seriellen Stecker

//serial_plug_test();

// Erstelle das gesamte Bauteil mit allen Submodulen (ohne Deckel)

bauteil_mit_allen_modulen();

// Teste die Passung des Deckels für IR-Kompartment, Parameter = Abstand vom Modul

//deckel_test_ir(0.5);

//Teste die Passung des Deckels für Elektronikkompartment, Parameter = Abstand vom Modul

//deckel_test(0);

// Erstelle IR Modul

//ir_module();

// Erstelle Bauteil ohne IR Modul

//bauteil_ohne_deckel();

// Erstelle Deckel für Elektronikkompartment

//deckel_gesamt();

// Erstelle Deckel für IR Modul

//deckel_ir_modul_mit_bohrungen();