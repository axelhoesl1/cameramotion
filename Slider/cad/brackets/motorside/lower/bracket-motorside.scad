$fn=300;

// Protusion (buffer)
prot = 0.15;

// Durchmesser Zugabe
z = 0.3;

// Abmessungen: C1 (Welle)
c1_r = 6.5/2.0 + z;
c1_h = 4.0;

// Abmessungen: C2 (Puffer)
c2_r = 15.0/2.0;
c2_h = 3.0;

// Abmessungen: C3 (Kugellager)
c3_r = 19.0/2.0;
c3_h = 6.0;

// Überlapp

overlap_laenge = 15;
overlap_dicke = 4;


// Abmessungen: Nema Bohrbild
nema_d    	= 47.14;
nema_r	  	= 3.0/2.0 + z;
nema_outline = 56.4;
bohrbild_tiefe = 30;

// Abmessungen: Kabelkanal

breite_kabelkanal = 11.0;
hoehe_kabelkanal = 4.30+prot;

//Abmessungen: Schiene

schiene_breite = 54;
schiene_breite_innen= 36.5;
schiene_h_basis = 7.5;
schiene_d_innen = 10;
schiene_d_aussen = 16;
abstand_tubes = 58;

//Abmessungen allgemein

dicke_bodenplatte = 13;
laenge_bodenplatte = nema_outline+overlap_laenge+30;
modullaenge = 50;
sockelgroesse=4-z*1.3;
korrekturfaktor_schiene=1.0;

// Schutz
l_schutz = 3.00; 
b_schutz = 20.00;
h_schutz = 2.00;

l_schutz_deckel = 22.00;
b_schutz_deckel = b_schutz + 1.5;
h_schutz_deckel = hoehe_kabelkanal-prot*2;

r_schutz_bohrung = (3.00)/2+z;
h_schutz_bohrung = dicke_bodenplatte-hoehe_kabelkanal-2;
d_schutz_bohrung = 3.5; 


//MODULE


//Wellenlauf

module wellenlauf(c1_r, c1_h, c2_r, c2_h, c3_r, c3_h){

    translate([0,0,-prot]) {
        cylinder(r=c1_r, h=c1_h+2*prot);
    }
	
	translate([0,0,c1_h]){
		cylinder(r=c2_r, h=c2_h+prot);
	}
	translate([0,0,c1_h+c2_h]){
		cylinder(r=c3_r, h=c3_h+prot);
	}
}

//Bohrbild 

//Bohrbild Verschiebung
//schiene_breite/2,dicke_bodenplatte+schiene_d_aussen+overlap_dicke-bohrbild_tiefe

module bohrbild(nema_r, nema_d, bohrbild_tiefe){
	translate([(nema_outline/2)+1,schiene_breite/2,dicke_bodenplatte+schiene_d_aussen+overlap_dicke/2-korrekturfaktor_schiene+modullaenge/3-bohrbild_tiefe+8]){
		for ( i = [0 : 3] ){
			rotate( 45+(i* 360 / 4), [0, 0, 1]){
    			translate([0, (nema_d/2)*sqrt(2), 0]){
    				cylinder (r = nema_r, h = bohrbild_tiefe+prot);
				}
			}
		}
	}
}


//Body (Hauptteil)

module body(laenge_bodenplatte, schiene_breite, dicke_bodenplatte, schiene_d_aussen, overlap_dicke){

//Sockel

	translate([(nema_outline)/2+1, schiene_breite/2,dicke_bodenplatte+schiene_d_aussen+overlap_dicke/2-prot-korrekturfaktor_schiene+8]){
		for ( i = [0 : 3]){
    		rotate( (i * 360 / 4)+45, [0, 0, 1]){
				translate([0, (nema_outline/2+sockelgroesse/2), 0]){
					rotate(45,[0,0,1]){
						hull(){
							translate([-sockelgroesse,-sockelgroesse,0]){
								cylinder(r=2, h=modullaenge/3-z);
							}
							translate([-sockelgroesse,sockelgroesse,0]){
								cylinder(r=2, h=modullaenge/3-z);
							}
							translate([sockelgroesse,-sockelgroesse,0]){
								cylinder(r=2, h=modullaenge/3-z);
							}
							translate([sockelgroesse,sockelgroesse,0]){
								cylinder(r=2, h=modullaenge/3-z);
							}
						}
					}
				}	
			}
		}
	}



hull(){

// Unterer eckiger Teil bei dem die Breite auf der Schienenbreite basiert

	cube([laenge_bodenplatte, schiene_breite, dicke_bodenplatte+schiene_d_aussen+overlap_dicke/2-prot-korrekturfaktor_schiene]);
	
// Oberer eckiger Teil, hinzugefügt um die notwendige Höhe für das Zahnrad zu erreichen. Die Breite orientiert sich an der Breite des Kupplungsmoduls um sauber anzuschliessen. Die z-Koordinate des folgenden cube bestimmt den Platz im inneren des Zahnradraumes. Anpassung der Sockelhöhe bei Änderung erforderlich!

	translate([0,-(nema_outline-schiene_breite)/2,dicke_bodenplatte+schiene_d_aussen+overlap_dicke/2-prot-korrekturfaktor_schiene]){
       cube([laenge_bodenplatte, nema_outline, 8]);
	}

// Gerundete Teile

	translate([0,-2,dicke_bodenplatte+schiene_d_aussen/2-korrekturfaktor_schiene]){
		rotate(90, [0,1,0]){
			cylinder(r=(schiene_d_aussen+overlap_dicke)/2, h=laenge_bodenplatte);
		}
	}
	translate([0,schiene_breite+2,dicke_bodenplatte+schiene_d_aussen/2-korrekturfaktor_schiene]){
		rotate(90, [0,1,0]){
			cylinder(r=(schiene_d_aussen+overlap_dicke)/2, h=laenge_bodenplatte);
		}
	}
    
    /*
    translate([0,schiene_breite+9,dicke_bodenplatte+schiene_d_aussen+6]){
		rotate(90, [0,1,0]){
			cylinder(r=3, h=nema_outline+5);
		}
	}
    */
}
}


//Ausschnitt oben

module ausschnitt_oben(){
	translate([(schiene_breite_innen/2)+schiene_h_basis,(schiene_breite/2) -(schiene_breite_innen/2),dicke_bodenplatte]){
		cube([laenge_bodenplatte,schiene_breite_innen,schiene_d_aussen+overlap_dicke/2+prot+modullaenge/2]);
	}
	translate([(schiene_breite_innen/2)+schiene_h_basis,schiene_breite/2,dicke_bodenplatte]){
		cylinder(r = schiene_breite_innen/2, h= schiene_d_aussen+overlap_dicke/2+prot+8);
	}
}

//Ausschnitt vorne

module ausschnitt_vorne(){

//Schraubengänge	

		translate([-prot,-2,dicke_bodenplatte+schiene_d_aussen/2-korrekturfaktor_schiene]){
			rotate(90, [0,1,0]){
				cylinder(r=schiene_d_innen/2+z, h=laenge_bodenplatte+prot);
			}
		}
		translate([-prot,schiene_breite+2,dicke_bodenplatte+schiene_d_aussen/2-korrekturfaktor_schiene]){
			rotate(90, [0,1,0]){
				cylinder(r=schiene_d_innen/2+z, h=laenge_bodenplatte+prot);
			}
		}

//Schienenfassungen

	//Röhren
		translate([laenge_bodenplatte-overlap_laenge,-2,dicke_bodenplatte+schiene_d_aussen/2-korrekturfaktor_schiene]){
			rotate(90, [0,1,0]){
				cylinder(r=z+(schiene_d_aussen/2), h=overlap_laenge+prot);
			}
		}
		translate([laenge_bodenplatte-overlap_laenge,schiene_breite+2,dicke_bodenplatte+schiene_d_aussen/2-korrekturfaktor_schiene]){
			rotate(90, [0,1,0]){
				cylinder(r=z+(schiene_d_aussen/2), h=overlap_laenge+prot);
			}
		}
	//Bodenplatten
		translate([laenge_bodenplatte-overlap_laenge,-z,dicke_bodenplatte-schiene_h_basis-z/2]){
			cube([overlap_laenge+prot,schiene_breite+2*z,dicke_bodenplatte-schiene_h_basis+schiene_d_aussen/2+overlap_dicke/2+z]);
		}

}

module ausschnitt_schutz() {
    translate([49,schiene_breite/2-b_schutz/2-prot,-prot]) {
        translate([z/4,-z/4,0])
            cube([l_schutz+z,b_schutz+z,dicke_bodenplatte+2*prot]);
        
        translate([-l_schutz_deckel/2+l_schutz/2-z/2,0,0]) {
           cube([l_schutz_deckel+2*z,b_schutz_deckel+z,h_schutz_deckel+2*prot]);
        }
        
        translate([r_schutz_bohrung/2+r_schutz_bohrung/2,l_schutz_deckel/2+r_schutz_bohrung/2,h_schutz_deckel]) {
            translate([l_schutz/2+r_schutz_bohrung/2+d_schutz_bohrung,0,0]){
                cylinder(r=r_schutz_bohrung,h=h_schutz_bohrung);
            }
            translate([-(l_schutz/2+r_schutz_bohrung/2+d_schutz_bohrung),0,0]){
                cylinder(r=r_schutz_bohrung,h=h_schutz_bohrung);
            }
        } 
    }   
}
// 
module schutz() {
translate([laenge_bodenplatte/2-l_schutz/2,schiene_breite/2-b_schutz/2-prot,h_schutz+prot]) {
 difference() {
        union() {
            cube([l_schutz,b_schutz,dicke_bodenplatte+schiene_d_aussen+overlap_dicke/2+modullaenge/2+38]);
            
            translate([-l_schutz_deckel/2+l_schutz/2,0,0]) {
               cube([l_schutz_deckel,b_schutz_deckel,h_schutz]);
            }
        }
    
        union() {
            translate([r_schutz_bohrung/2+r_schutz_bohrung/2,l_schutz_deckel/2+r_schutz_bohrung/2,0]) {
                translate([l_schutz/2+r_schutz_bohrung/2+d_schutz_bohrung,0,-prot]){
                    cylinder(r=r_schutz_bohrung,h=h_schutz_bohrung);
                }
                translate([-(l_schutz/2+r_schutz_bohrung/2+d_schutz_bohrung),0,-prot]){
                    cylinder(r=r_schutz_bohrung,h=h_schutz_bohrung);
                }
            } 
        }
  }
  }
}

module body_oben(){
	difference(){
		body(laenge_bodenplatte, schiene_breite, dicke_bodenplatte, schiene_d_aussen, overlap_dicke);
		ausschnitt_oben();
	}
}

module body_komplett(){
	difference(){
		body_oben();
		ausschnitt_vorne();
        ausschnitt_schutz();
	}
}

//der folgendene Korrekturfaktor auf der x-Achse kommt durch die Verschiebung (nema_outline/2)+1 im Kupplungsmodul zu stande

module body_welle(){
	difference(){
		body_komplett();
		translate([(nema_outline/2)+1,schiene_breite/2,0]){
			wellenlauf(c1_r, c1_h, c2_r, c2_h, c3_r, c3_h);
		}
        
	}
}

// Kabelkanal unter dem Endteil

module kabelkanal(){
	translate([-prot,schiene_breite/4-breite_kabelkanal/2,-prot]){
		cube([laenge_bodenplatte-27.5+prot*2, breite_kabelkanal, hoehe_kabelkanal]);
	}
	translate([laenge_bodenplatte-35,11.2,-prot]){
		rotate(-45,[0,0,1]){
			cube([35, breite_kabelkanal, hoehe_kabelkanal]);
		}
	}
}

module endteil_mit_kabelkanal(){
	difference(){
		body_welle();
		kabelkanal();
	}
}

// Hinzufügen des Bohrbilds

module endteil_ohne_schalter(){
	difference(){
		endteil_mit_kabelkanal();
		bohrbild(nema_r, nema_d, bohrbild_tiefe);
	}
}


//Teilmodul Schalterhalter inklusive lokaler Variablen für Submodul

//Abmessungen Schalter

dicke = 6.80+z;

laenge_seg_oben = 19.35+z;
laenge_seg_mitte = 18.15+z;
laenge_seg_unten = 12.70+z;
laenge_seg_boden = laenge_seg_unten;

hoehe_seg_oben = 4.00+z;
hoehe_seg_mitte = 2.30+z;
hoehe_seg_unten = 6.30+z;
hoehe_seg_boden = 4.00+z;

radius_bolzen = 1.45+z/2;
laenge_bolzen = 9.75;

radius_bohrung = 1.60+z/2;

// Abmessungen Halterung
 
wanddicke = 1.50;

hoehe_halterung = hoehe_seg_oben+hoehe_seg_mitte+hoehe_seg_unten+hoehe_seg_boden;
laenge_halterung = laenge_seg_oben+wanddicke*2;
dicke_halterung = laenge_bolzen+wanddicke*2;

// Hauptkörper, umfasst den Schalter

module schalter(){

	// Laengeres Teilsegment oben

	translate([0, 0, hoehe_seg_unten+hoehe_seg_boden+hoehe_seg_mitte]){
		cube ([laenge_seg_oben, dicke, hoehe_seg_oben]);
	}

	// Mittelsegment

	translate([laenge_seg_oben-laenge_seg_mitte, 0, hoehe_seg_unten+hoehe_seg_boden]){
		cube ([laenge_seg_mitte, dicke, hoehe_seg_mitte+prot]);
	}

	// Kürzeres Teilsegment unten

	translate([laenge_seg_oben-laenge_seg_mitte, 0, hoehe_seg_boden]){
		cube ([laenge_seg_unten, dicke, hoehe_seg_unten+prot]);
	}

	// Puffersegment unten für Kabelausgang

	translate([laenge_seg_oben-laenge_seg_mitte, 0, -prot*2]){
		cube([laenge_seg_boden, dicke, hoehe_seg_boden+prot*4]);
	}

	// Bolzen

	translate([laenge_seg_oben-laenge_seg_mitte+radius_bolzen,laenge_bolzen-((laenge_bolzen-dicke)/2),hoehe_seg_boden+hoehe_seg_unten+radius_bolzen+0.5]){
		rotate(90,[1,0,0]){
			cylinder(r = radius_bolzen, h= laenge_bolzen);
		}	
	}
	translate([laenge_seg_oben-laenge_seg_mitte, -((laenge_bolzen-dicke)/2), hoehe_seg_boden+hoehe_seg_unten+radius_bolzen+0.5-prot]){
		cube([radius_bolzen*2, laenge_bolzen, hoehe_seg_oben+radius_bolzen+prot]);
	}
}

// Achtung: Durch die Minkowski Summe vergroessert sich der Container, was bei der Laenge der Bohrung beachtet werden muss!

module container(){

		translate([-wanddicke, -(dicke_halterung/2)+dicke/2-5, -prot]){
			minkowski(){
				cube([laenge_halterung, dicke_halterung+5, hoehe_halterung]);
				cylinder(r = 1, h = 0.01);
			}
		}

}


module bohrung (){

	translate([laenge_seg_oben-radius_bohrung-2.00, (dicke_halterung/2)+dicke/2+2, hoehe_seg_boden+hoehe_seg_unten+hoehe_seg_mitte+radius_bohrung-1.00]){
		rotate(90,[1,0,0]){
			cylinder ( r = radius_bohrung, h = dicke_halterung+prot*2+3);
            cylinder(r1 = radius_bohrung+3, r2= radius_bohrung, h=2);
		}
	}
}

module schalter_ohne_bohrung(){
	difference(){
		container();
		schalter();
	}
}

module schalter_komplett(){
	rotate(180,[0,0,1]){
		difference(){
			schalter_ohne_bohrung();
			bohrung();
		}
	}
}

// Die Schalterverschiebung ist ein empirischer Wert
schalterverschiebung = 15.50;

// Die z Verschiebung ist ein empirischer Wert

// Das -1 im translate kommt durch den Radius 1 des Zylinders in der Minkowski Summe

module schalter_mit_sockel(){
	difference(){
		schalter_komplett();
		translate([-laenge_halterung+0.5,dicke+wanddicke*3.5,hoehe_halterung/2+z]){
			rotate(90, [0,1,0]){
				cylinder(r=z+(schiene_d_aussen/2), h=laenge_halterung+wanddicke+0.5+prot);
			}
		}

	}
}

// Da die Teile chiral sind braucht man für die beiden Enden der Schiene verschiedene Versionen. Dies ist die Version mit dem Schalter auf der linken Seite (von vorne darauf blickend)

module endteil_mit_schalter_links(){
	endteil_ohne_schalter();
	translate([laenge_bodenplatte-wanddicke-1,-schalterverschiebung,dicke_bodenplatte-2.5]){
		schalter_mit_sockel();
	}
}

endteil_mit_schalter_links();

//schutz();




