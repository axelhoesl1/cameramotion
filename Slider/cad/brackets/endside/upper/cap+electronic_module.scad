$fn=300;

//Zugabe

z = 0.3;
prot = 0.15;

//Abmessungen allgemein
teillaenge = 66.4;
modullaenge = 50;
sockelgroesse = 4+z/2;
oberer_teil = 13.00;


// Abmessungen: C3 (Kugellager)
c3_r = 19.0/2.0 + z;
c3_h = 6.0;

// Abmessungen: C2 (Puffer)
c2_r = 15.0/2.0 + z;
c2_h = 3.0;

// Abmessungen: C1 (Welle)
c1_r = 6.50/2.0;
c1_h = c2_h;

// Abmessungen: Nema Bohrbild
nema_d    	= 47.14;
nema_r	  	= 3.0/2.0 + z;
nema_outline = 56.4;
nema_motorkern = 38.1;

r_schraube_versenkung = (7.70+z)/2;

// Abmessungen Elektronikkompartment

deckel = 2.00;
deckel_innen = 1.00;
wandstaerke = 9.00;
laenge_kompartment = teillaenge+2-wandstaerke;
breite_kompartment = nema_outline+2-wandstaerke*2;
hoehe_kompartment = 12.00;

r_kabeldurchgang = (9.00+z)/2;
h_kabeldurchgang = wandstaerke+2*prot;

// Befestigung der Platine

r_boardschrauben_ir = (1.60+2*z)/2;
h_boardschrauben_ir = 6.00;
breite_sockel_board_ir = 5.00;
hoehe_sockel_board_ir = 5.00;
randabstand_sockel_board_seitlich_ir = 4.00;
randabstand_sockel_board_hinten_ir = 20.00;

//MODULE


//Wellenlauf

module wellenlauf(c1_r, c1_h, c2_r, c2_h, c3_r, c3_h){

    translate([0,0,-prot]) {
	cylinder(r=c3_r, h=c3_h+2*prot);
	}
	translate([0,0,c3_h]){
		cylinder(r=c2_r, h=c2_h+prot);
	}
	translate([0,0,c3_h+c2_h]){
		cylinder(r=c1_r, h=c1_h+prot);
	}
}

//Hauptkörper

module body(){
	translate([2,2,0]){
		hull(){
		cylinder(r=2, h=modullaenge/3+oberer_teil);
		translate([teillaenge-2,0,0]){
            cylinder(r=2, h=modullaenge/3+oberer_teil);
		}
		translate([0,nema_outline-2,0]){
            cylinder(r=2, h=modullaenge/3+oberer_teil);
		}
		translate([teillaenge-2,nema_outline-2,0]){
            cylinder(r=2, h=modullaenge/3+oberer_teil);
		}
		
		}
	}
}

//Ausschnitte

module ausschnitte(){

// Wellenlauf

	translate([nema_outline/2+1,nema_outline/2+1,0]){
		wellenlauf(c1_r, c1_h, c2_r, c2_h, c3_r, c3_h);
	}



// Bohrlöcher
	translate([nema_outline/2+1, nema_outline/2+1, 0]){
		for ( i = [0 : 3]){
    		rotate( (i * 360 / 4)+45, [0, 0, 1]){
    			translate([0, (nema_d/2)*sqrt(2), 0]){
   					cylinder(r = nema_r, h=modullaenge+prot);
				}
			}	
		}
	}

//Sockel
	translate([nema_outline/2+1, nema_outline/2+1,-prot]){
		for ( i = [0 : 3]){
    		rotate( (i * 360 / 4)+45, [0, 0, 1]){
				translate([0, (nema_outline/2+2), 0]){
					rotate(45,[0,0,1]){
						hull(){
							translate([-sockelgroesse,-sockelgroesse,-prot]){
								cylinder(r=2, h=2*z+modullaenge/3);
							}
							translate([-sockelgroesse,sockelgroesse,-prot]){
								cylinder(r=2, h=2*z+modullaenge/3);
							}
							translate([sockelgroesse,-sockelgroesse,-prot]){
								cylinder(r=2, h=2*z+modullaenge/3);
							}
							translate([sockelgroesse,sockelgroesse,-prot]){
								cylinder(r=2, h=2*z+modullaenge/3);
							}
						}
					}
				}	
			}
		}
	}

//Ausschnitt für Elektronikkompartment

	translate([teillaenge+2-laenge_kompartment,nema_outline+2-breite_kompartment-wandstaerke,modullaenge/3+oberer_teil-hoehe_kompartment]){
		cube([laenge_kompartment+prot, breite_kompartment, hoehe_kompartment+prot]);
	}

// Ausschnitt für Kabel 1
	translate([-prot,(nema_outline+2)*2/3,oberer_teil+modullaenge/3]){
		rotate(90,[0,1,0]){
			cylinder(r=r_kabeldurchgang, h=h_kabeldurchgang);
		}
	}
    
    // Ausschnitt für Kabel 2
translate([-prot,(nema_outline+2)*1/3,oberer_teil+modullaenge/3]){
		rotate(90,[0,1,0]){
			cylinder(r=r_kabeldurchgang, h=h_kabeldurchgang);
		}
	}
}


module body_mit_ausschnitten(){
	difference(){
		body();
		ausschnitte();
	}
}

module deckel(){
	difference(){
		body();
        translate([-prot,-prot,-prot]){
            cube([teillaenge+2+2*prot, nema_outline+2+2*prot, modullaenge/3+oberer_teil-deckel+prot]);
        }
	}
}

module deckel_mit_bohrung(){
	translate([0,0,30]){
		difference(){
			deckel();
			translate([nema_outline/2+1, nema_outline/2+1, 0]){
				for ( i = [0 : 3]){
    				rotate( (i * 360 / 4)+45, [0, 0, 1]){
    					translate([0, (nema_d/2)*sqrt(2), modullaenge/3+oberer_teil-deckel-prot]){
   						cylinder(r1 = nema_r, r2= r_schraube_versenkung, h=deckel+2*prot);
						}
					}	
				}
			}
		}
	}
}

//Boardsockel

module sockel_board(){

	difference(){
		cube([breite_sockel_board_ir, breite_sockel_board_ir, hoehe_sockel_board_ir]);
		translate([breite_sockel_board_ir/2,breite_sockel_board_ir/2,prot]){
			
				cylinder( r = r_boardschrauben_ir,h = h_boardschrauben_ir);
			
		}
	}

//Translation für IR Board spezifisch

	translate([17.5,27.7,0]){
		difference(){
			cube([breite_sockel_board_ir, breite_sockel_board_ir, hoehe_sockel_board_ir]);
			translate([breite_sockel_board_ir/2,breite_sockel_board_ir/2,prot]){
				
					cylinder( r = r_boardschrauben_ir,h = h_boardschrauben_ir);
				
			}
		}
	}
}

module kappe_mit_elektronikmodul(){
	translate([wandstaerke+randabstand_sockel_board_hinten_ir, wandstaerke+randabstand_sockel_board_seitlich_ir, modullaenge/3+oberer_teil-hoehe_kompartment-0.5]){
		sockel_board();
	}
	body_mit_ausschnitten();
}


// Erzeuge komplettes Modul

//kappe_mit_elektronikmodul();

// Erzeuge Deckel

deckel_mit_bohrung();