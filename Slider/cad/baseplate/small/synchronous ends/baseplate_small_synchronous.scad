//Parameter

$fn = 300;
prot = 0.15;
z = 0.30;

//Allgemeine Variablen

laenge_schlitten = 100.00+prot;
breite_schlitten = 104.00+prot;
hoehe_schlitten = 33.50;
hoehe_oberteil = 7.90;
hoehe_platte = 10.00;

//Abmessungen Bohrung Befestigung
r_bohr = (8.00)/2;
r_schraubkopf = (14.00)/2;
h_schraubkopf = 8.00;

r_kern = (10.00+z)/2;
h_kern = hoehe_platte;

dist_bohr_laengs = 82.00;
dist_bohr_breit = 86.00;

//Abmessungen Bohrung Durchlass

r_durchlass = (12.80)/2;
h_durchlass = hoehe_platte+2*prot;
dist_bohr_laengs_durchlass = dist_bohr_laengs-2*18.00;
dist_bohr_breit_durchlass = dist_bohr_breit;

//Abmessungen Bohrung Gimbal
r_gimbal = (3.00+z)/2;
h_gimbal = hoehe_platte+2*prot;
dist_bohr_laengs_gimbal = 80.00;
dist_bohr_breit_gimbal = 30.00;

//Abmessungen Stift

r_stift = (15.00)/2;
l_stift_ir_seite = 25.00;
l_stift_motor_seite = 25.00;
b_stift = 10.00;
h_stift = 10.00;
rundung_stift = 5;

//Abmessungen Freiraum für Schrauben von unten

laenge_freiraum = 25.00;
breite_freiraum = 10.00;
hoehe_freiraum = hoehe_platte - 2.50;

//Abmessungen Reflektor

laenge_reflektor = 20.00;
breite_reflektor = 20.00;
hoehe_reflektor = 3.50;
hoehe_reflektorsockel = 1.50;
reflektor_kruemmung = 30.00;

reflektor_neigung_laengs = -25.00;
reflektor_neigung_quer = 15.00;

//Bohrungen für Befestigung am Schlitten

module bohrung(){

//4 Durchlässe für die Schrauben der Gleitelemente

translate([(laenge_schlitten-dist_bohr_laengs_durchlass)/2,(breite_schlitten-dist_bohr_breit_durchlass)/2,0]){
		translate([0,0,-prot]) {
            cylinder(r = r_durchlass, h=h_durchlass);
        }
		translate([dist_bohr_laengs_durchlass,0,-prot]){
			cylinder(r = r_durchlass, h=h_durchlass);
			
		}
	}
	translate([(laenge_schlitten-dist_bohr_laengs_durchlass)/2,(breite_schlitten-dist_bohr_breit_durchlass)/2+dist_bohr_breit_durchlass,0]){
        translate([0,0,-prot]) {
            cylinder(r = r_durchlass, h=h_durchlass);
		}
		translate([dist_bohr_laengs_durchlass,0,-prot]){
			cylinder(r = r_durchlass, h=h_durchlass);
			
		}
	}
	
// 4 Schrauben mit Sockelversenkung in den Ecken

	translate([(laenge_schlitten-dist_bohr_laengs)/2,(breite_schlitten-dist_bohr_breit)/2,0]){
		translate([0,0, hoehe_platte-h_schraubkopf]){
			cylinder (r = r_schraubkopf, h = h_schraubkopf+prot);
		}
		translate([0,0,-prot]) {
            cylinder(r = r_bohr, h=h_durchlass);
        }
		translate([dist_bohr_laengs,0,0]){
            translate([0,0,-prot]) {
                cylinder(r = r_bohr, h=h_durchlass);
            }
			translate([0,0, hoehe_platte-h_schraubkopf]){
				cylinder (r = r_schraubkopf, h = h_schraubkopf+prot);
			}
		}
	}
	translate([(laenge_schlitten-dist_bohr_laengs)/2,(breite_schlitten-dist_bohr_breit)/2+dist_bohr_breit,0]){
        translate([0,0,-prot]) {
            cylinder(r = r_bohr, h=h_durchlass);
        }
		translate([0,0, hoehe_platte-h_schraubkopf]){
			cylinder (r = r_schraubkopf, h = h_schraubkopf+prot);
		}

		translate([dist_bohr_laengs,0,0]){
            translate([0,0,-prot]) {
                cylinder(r = r_bohr, h=h_durchlass);
            }
			translate([0,0, hoehe_platte-h_schraubkopf]){
				cylinder (r = r_schraubkopf, h = h_schraubkopf+prot);
			}
		}
	}


// Kerndurchlass

	translate([laenge_schlitten/2, breite_schlitten/2, -prot]){
		cylinder( r = r_kern, h = h_kern+2*prot);
	}

//4 Durchlässe für die Schrauben des Gimbal

translate([(laenge_schlitten-dist_bohr_laengs_gimbal)/2,(breite_schlitten-dist_bohr_breit_gimbal)/2,0]){
		translate([0,0,-prot]) {
            cylinder(r = r_gimbal, h=h_gimbal);
        }
		translate([dist_bohr_laengs_gimbal,0,-prot]){
			cylinder(r = r_gimbal, h=h_gimbal);
			
		}
	}
	translate([(laenge_schlitten-dist_bohr_laengs_gimbal)/2,(breite_schlitten-dist_bohr_breit_gimbal)/2+dist_bohr_breit_gimbal,0]){
        translate([0,0,-prot]) {
            cylinder(r = r_gimbal, h=h_gimbal);
		}
		translate([dist_bohr_laengs_gimbal,0,-prot]){
			cylinder(r = r_gimbal, h=h_gimbal);
			
		}
	}


// Freiraum für von unten kommende Befestigungsschrauben
	
	translate([3.50, 52, -prot]){
		cube([laenge_freiraum, breite_freiraum, hoehe_freiraum]);
	}

	translate([72.00, 52, -prot]){
		cube([laenge_freiraum, breite_freiraum, hoehe_freiraum]);
	}

}

//Stiftmodul A

module stift_a(){
translate([0,0,-hoehe_platte*3/4-1]){
	translate([-l_stift_motor_seite/2+prot, b_stift/2,hoehe_platte/2]){
		rotate(180,[1,0,0]){
			difference() {
				cube([l_stift_motor_seite, b_stift, h_stift], center = true);
		
				translate([-l_stift_motor_seite/2+r_stift, 0, h_stift/2-r_stift]) {
					difference() {
						translate([-r_stift+prot,0,r_stift+prot]){
							cube([r_stift*2+prot, b_stift+2*prot, r_stift*2+prot], center = true);
						}
						rotate(a=[90,0,0]){
							cylinder(b_stift+4*prot,r_stift, r_stift,center=true);
						}
			
					}
				}
			}
		}
	}

	translate([laenge_schlitten-prot,b_stift,0]){
		rotate (180,[0,0,1]){
			translate([-l_stift_ir_seite/2+prot, b_stift/2,h_stift/2]){
				rotate(180,[1,0,0]){
					difference() {
						cube([l_stift_ir_seite, b_stift, hoehe_platte], center = true);
		
						translate([-l_stift_ir_seite/2+r_stift, 0, h_stift/2-r_stift]) {
							difference() {
								translate([-r_stift+prot,0,r_stift+prot]){
									cube([r_stift*2+prot, b_stift+2*prot, r_stift*2+prot], center = true);
								}
								rotate(a=[90,0,0]){
									cylinder(b_stift+4*prot,r_stift, r_stift,center=true);
								}
			
							}
						}
					}
				}
			}
		}
	}
}
}
//Stiftmodul B (andere Rundungsform) (wird nicht verwendet, Variablen müssen davor angepasst werden!

module stift_b(){
	
	hull(){
		cube([l_stift, b_stift, hoehe_oberteil], center = true);
		translate([0,0,hoehe_oberteil/2+dicke_platte/2]){
			cube([l_stift, b_stift, dicke_platte], center = true);
		}
		translate([-(l_stift/2)-rundung_stift,0,1]){	
			rotate(a=[90,0,0]){
				cylinder(b_stift,r_stift*0.7, r_stift*0.7,center=true);
			}
		}
	}
}

module grundplatte(){
	difference(){
		cube([laenge_schlitten,breite_schlitten,hoehe_platte]);
		bohrung();
	}
}

module grundplatte_mit_trigger(){
	grundplatte();
	stift_a();
}


module ir_reflektor(){

	//rotate(reflektor_neigung_quer, [1,0,0]){
		rotate(reflektor_neigung_laengs, [-1,2,0], center = true){

			translate([0,0,prot/2]){
				cube(size = [laenge_reflektor,breite_reflektor,hoehe_reflektorsockel], center = true);
			}
			translate([0,0,-reflektor_kruemmung+hoehe_reflektor+hoehe_reflektorsockel/2]){
				intersection(){
					translate([0,0,reflektor_kruemmung-hoehe_reflektor/2]){
						cube(size = [laenge_reflektor,breite_reflektor,hoehe_reflektor], center = true);
					}
					sphere(r=reflektor_kruemmung);
				}
			}
		}
	//}
}

module komplett(){
	grundplatte_mit_trigger();

	translate([laenge_schlitten*4/5, breite_schlitten+breite_reflektor/2-2*z, 7-2*z]){
		ir_reflektor();
	}
}

grundplatte_mit_trigger();
//komplett();

module slope(){

	translate([-l_stift_motor_seite/2+prot,b_stift/2,(hoehe_platte*3/4+1+prot)/2+hoehe_platte*1/4-1]){
		rotate(-(atan((hoehe_platte*3/4+1+prot)/(l_stift_motor_seite))),[0,1,0]){
			difference(){
				rotate(atan((hoehe_platte*3/4+1+prot)/(l_stift_motor_seite)),[0,1,0]){
					cube([l_stift_motor_seite, b_stift, hoehe_platte*3/4+1+prot], center = true);
				}
				translate([0,0,(hoehe_platte*3/4+1+prot)/2]){
					cube([l_stift_motor_seite+5, b_stift, hoehe_platte*3/4+1+prot], center = true);
				}
			}
		}
	}
	translate([laenge_schlitten-prot+l_stift_ir_seite/2,b_stift/2,hoehe_platte*1/4-1]){
		rotate(180, [0,0,1]){
			translate([0,0,(hoehe_platte*3/4+1+prot)/2]){
				rotate(-(atan((hoehe_platte*3/4+1+prot)/(l_stift_ir_seite))),[0,1,0]){
					difference(){
						rotate(atan((hoehe_platte*3/4+1+prot)/(l_stift_ir_seite)),[0,1,0]){
							cube([l_stift_ir_seite, b_stift, hoehe_platte*3/4+1+prot], center = true);
						}
						translate([0,0,(hoehe_platte*3/4+1+prot)/2]){
							cube([l_stift_ir_seite+5, b_stift, hoehe_platte*3/4+1+prot], center = true);
						}
					}
				}
			}
		}
	}
}
slope();

