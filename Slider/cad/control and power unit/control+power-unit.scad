/*

	Design of battery pack container and the main control unit for the camera slider.
	Author: 	Patrick Mörwald, LMU München
				patrick.j.moerwald@campus.lmu.de

	Depends on:

	arduino.scad -> include
	(pins.scad -> include)
	DB9Mount.stl -> import
*/

include <arduino.scad>
//include <pins.scad>
// General parameters

$fn = 30;
//$fa = 1.0; //Minimum angle of facets
//$fs =0.5; //Minimum size of facets, 1.0 is rough, 0.01 very smooth
prot = 0.15;
z = 0.30;

/*

TODO: 	
		Beschriftungen
		Lade Interface modularisieren
		

*/

//*****************//
//OBJECT DIMENSIONS//
//*****************//

//Dimensions case of battery unit

case_rounding = 5.00;
case_wall = 7.50;
case_feet_female = 20.00;
case_feet_male = 20.00 - 2*z;
case_feet_hoehe = 7.50;
case_top_hoehe = 7.50;
case_top_feet_space = 2.50;

case_hoehe = 190.00;
case_laenge = 190.00;
case_breite = 190.00;
case_laenge_pre_minkowski = case_laenge - case_rounding;
case_breite_pre_minkowski = case_laenge - case_rounding;

//Dimensions case of control unit

controlcase_laenge = case_laenge;
controlcase_breite = case_breite;
controlcase_hoehe = 100.00;
controlcase_laenge_pre_minkowski = case_laenge - case_rounding;
controlcase_breite_pre_minkowski = case_laenge - case_rounding;
controlcase_top_hoehe = 7.50;

//Dimensions of snappers to attach controlcase top

snapper_laenge = 10.00;
snapper_breite = 3.00;
snapper_hoehe = 15.00;
d_snapper = 3.00;
snapper_looseness = 0.50;

//Dimensions of lift hole to help detach controlcase top lid

controlcase_lift_hole_breite = 15.00;

//Dimensions of the securing brim for the controlcase top lid

controlcase_brim_hoehe = 2.00;
controlcase_brim_breite = 2.00;

//Dimensions Battery Multipower MP7.2-12 PB

bat_laenge = 151.00 + z;
bat_breite = 65.00 + z;
bat_hoehe = 94.00 + z;
bat_offset_connectors_side = 7.50;
bat_offset_connectors_front = 14.00;
bat_breite_connectors = 4.80;
bat_laenge_connectors = 10.00;
bat_hoehe_connectors = 5.00;

//Dimensions battery tray

tray_wall_breite = 5.00;
tray_laenge = bat_laenge + tray_wall_breite*2;
tray_breite = bat_breite*2 + tray_wall_breite*3;
tray_hoehe = bat_hoehe + tray_wall_breite*2;
tray_wall_cutout = tray_wall_breite + z;
tray_handle_height = 20.00 + tray_wall_breite;
tray_socket_hoehe = 2.00;

//Dimensions of beam lock for battery tray

beam_lock_laenge = case_laenge - case_wall/2;
beam_lock_breite = 30.00;
beam_lock_hoehe = 10.00;

//Dimensions of DIN Plug for LiYCY cable

d_din = 18.15 + z;
h_din_ges = 12.50 + z;
din_nose_laenge = 2.20 + z;
din_nose_breite = 1.40 + z;
din_nose_hoehe = 1.50 + z;
din_interface_tiefe = 4.50;
d_din_interface = 35.00;

//Dimensions of Coaxial DC Connector DIN 45323

laenge_dccoax_ges = 14.20 + z;
laenge_dccoax_free = 7.20;
breite_dccoax = 8.90 + z; //(w/out metal protrusions)
hoehe_dccoax = 10.90 + z;
hoehe_dcstopper_offset = 1.90;
laenge_dcstopper_offset = 3.50;
d_coax_interface = 35.00;
h_coax_interface = case_wall - laenge_dcstopper_offset - laenge_dccoax_free;

//Dimensions of DB9 connector (RS-232 interface)

db9_interface_tiefe = 2.50;
d_db9_interface = 45.00;

// Dimensions of screws
//-> Zylinderkopfschrauben kaufen
//M4x16

d_m4x16_body = 3.60; //This is abit smaller than the actual body to allow good grip
h_m4x16_body = 16.00;
d_m4x16_head = 6.80 + z;
h_m4x16_head = 2.75;

//M3x10

//ATTENTION: SOME DUMMY VALUES, JUST FOR DESIGNING!!
d_m3x10_body = 2.60; //This is abit smaller than the actual body to allow good grip
h_m3x10_body = 10.00;
d_m3x10_head = 4.80 + z;
h_m3x10_head = 1.75;

//Dimensions of board screws

d_boardscrew_body = 1.90 + z;
h_boardscrew_body = 7.00 + z;
d_boardscrew_head = 4.80 + z;
h_boardscrew_head = 1.60 + z;

//Dimensions of board screw socket

boardscrew_socket_wall = 2.50;

//Dimensions Boost Converter

boost_laenge = 85.00 + z;
boost_breite = 63.00 + z;
boost_hoehe = 65.00 + z;
boost_fins_laenge = 19.50 + z;
boost_radiator_laenge = 25.00 + z;

//Dimensions of Tamiya plug

tamiya_laenge_ges = 27.00;
tamiya_laenge_back_to_bolt = 13.70 + z;
tamiya_breite = 13.50 + z;
tamiya_hoehe = 7.25 + z;

tamiya_bolt_top_laenge = 1.25 + z;
tamiya_bolt_top_breite = 6.90 + z;
tamiya_bolt_top_hoehe = 1.50 + z;

tamiya_bolt_bottom_laenge = 1.25 + z;
tamiya_bolt_bottom_breite = 3.50 + z;
tamiya_bolt_bottom_hoehe = 1.50 + z;

//Dimensions of battery on/off switch

onoff_snapin_buffer = 0.50; //some buffer to make the snap in fit more easily
bat_onoff_laenge = 28.20 + z + onoff_snapin_buffer; 
bat_onoff_breite = 21.40 + z;
bat_onoff_hoehe = 16.50 + z;

bat_onoff_kranz_laenge = 30.60 + z;
bat_onoff_kranz_breite = 25.00 + z;
bat_onoff_kranz_hoehe = 2.15 + z;

//Dimensions of control case high voltage (motor supply) switch

mot_onoff_laenge = 29.50 + z + onoff_snapin_buffer;
mot_onoff_breite = 10.85 + z;
mot_onoff_hoehe = 16.50 + z;

mot_onoff_kranz_laenge = 31.50 + z;
mot_onoff_kranz_breite = 13.90 + z;
mot_onoff_kranz_hoehe = 2.00 + z;

//Dimensions of control case low voltage (logic supply) switch

logic_onoff_laenge = 18.50 + z + onoff_snapin_buffer;
logic_onoff_breite = 12.95 + z;
logic_onoff_hoehe = 10.50 + z;

logic_onoff_kranz_laenge = 20.90 + z;
logic_onoff_kranz_breite = 14.90 + z;
logic_onoff_kranz_hoehe = 2.00 + z;

//Dimensions of charging interface

d_charging_interface = 50.00;
h_charging_interface = tamiya_laenge_ges - tamiya_laenge_back_to_bolt + 4.00;
charger_wall_breite = 5.00;

//Dimensions of cap for charging interface

charger_cap_wall_breite = 7.50;
d_charger_cap_aussen = d_charging_interface + charger_cap_wall_breite;
d_charger_cap_innen = d_charging_interface - (charger_wall_breite - z)*2;
h_charger_cap_boden = 5.00;
h_charger_cap_ges = h_charging_interface + h_charger_cap_boden;
d_grip = 2.00;
num_grip = 24;

//Dimensions of DM556 motor driver

dm556_laenge = 76.00 + z*2;
dm556_breite = 118.00 + z*2;
dm556_hoehe = 34.00 + z*2;
dm556_dist_front_to_screw = 40.00 + z;
dm556_dist_side_to_screw = 3.00 + z;
dm556_breite_interval_screws = 112.00 + z;
dm556_kk_hoehe = 12.50 + z;
dm556_front_dicke = 3.50 + z;
dm556_pcb_breite = 101.50 + z;
dm556_dist_bottom_to_front_screw = 22.60 + z;

dm556_holder_laenge = 20.00;
dm556_holder_breite = 10.00;

dm556_bridge_hoehe = 10.00;
dm556_bridge_breite = 8.00;

//Dimensions for PCB mounting platform above DM556 driver

pcb_platform_laenge = 60.00;
pcb_platform_breite = controlcase_breite - case_wall*2 - z*2;
pcb_platform_hoehe = 7.00;
pcb_platform_cutout_laenge = 40.00;
pcb_platform_cutout_breite = 20.00; //(controlcase_breite - case_wall*2 - dm556_breite)/2 + dm556_holder_breite;
pcb_platform_screw_offset = (pcb_platform_laenge - pcb_platform_cutout_laenge)/4;
pcb_platform_board_spacing = 10.00; //Spacing between individual platform PCB mounts in the y-Dimension

//Dimension of PCB mounting platform holder

pcb_platform_holder_breite = (pcb_platform_laenge - pcb_platform_cutout_laenge)/2;

//Dimensions of OLED display

oled_laenge = 27.30 + z;
oled_breite = 27.00 + z;
oled_hoehe_ges = 2.60 + z;
oled_hoehe_screen = 1.60 + z;
oled_dist_screen_from_top = 5.00;
oled_display_laenge = 15.00 + z;
oled_display_breite = 27.00 + z;
oled_screenprotector = 0.00 + z; //Plexiglas shield

//Dimensions of driver status LED

driver_LED_laenge = 4.50 + z;
driver_LED_breite = 10.00 + z;
driver_LED_hoehe = 10.00 + z;

driver_LED_protrusion_breite = 2.00 + z;
driver_LED_protrusion_laenge = 2.90 + z;
driver_LED_protrusion_hoehe = 8.10 + z;

//Dimensions of controller status LED

controller_LED_protrusion_laenge = 3.00 + z;
controller_LED_protrusion_breite = 3.30 + z;
controller_LED_protrusion_hoehe = 2.90 + z;

controller_LED_laenge = 3.40 + z;
controller_LED_breite = case_wall - controller_LED_protrusion_breite + z;
controller_LED_hoehe = controller_LED_protrusion_hoehe;

//Dimensions of USB interface

usb_interface_laenge = 13.00;
usb_interface_hoehe = 10.00;
usb_interface_dist_ports = 16.00;

//Dimension of due socket

due_feet_hoehe = 7.00;

//Dimension of DUE board

due_dist_usb_from_bottom = 3.00;

//Dimensions of PCB screws//
//************************//

LLC_x_shift = 35.50 + z;
LLC_y_shift = 22.50 + z;

UNO_x_shift = 53.05 + z;
UNO_y_shift = 45.95 + z;

DEBOUNCE_x_shift = 32.80 + z;
DEBOUNCE_y1_shift = 17.70 + z;
DEBOUNCE_y2_shift = 15.15 + z;

STEPDOWN_x_shift = 38.50 + z;
STEPDOWN_y_shift = 16.50 + z;

DRIVER_INT_x_shift = 30.50 + z;
DRIVER_INT_y2_shift = 25.50 + z;

OLED_y_shift = 20.00 + z;
OLED_x_shift = 23.00 + z;

//**************************************//
//POSITIONING OF BATTERY CASE COMPONENTS//
//**************************************//

//Position of battery tray

tray_x_pos = case_wall;
tray_y_pos = case_wall;
tray_z_pos = case_wall - tray_socket_hoehe;

//Position of beam lock

beam_lock_x_pos = case_wall/2;
beam_lock_y_pos = tray_y_pos + tray_breite*2/3 + z;
beam_lock_z_pos = tray_z_pos + tray_hoehe + z;

//Position of Boost Converter

boost_x_pos = case_laenge/2 - 20.00;
boost_y_pos = case_breite;
boost_z_pos = case_hoehe  - 10.00;

//Position of DIN plug [maxpos = 190.00]

din_x_pos = case_laenge - 35.00;
din_y_pos = case_breite - case_wall - prot;
din_z_pos = case_hoehe - boost_breite/2;

//Position of DC coaxial plug on battery pack

coax_x_pos_bat = case_laenge - 35.00 - hoehe_dcstopper_offset/2;
coax_y_pos_bat = case_breite;
coax_z_pos_bat = case_hoehe - boost_breite - 10.00;

//Position of charging interface 

charging_interface_x_pos = case_laenge/2 + 20.00;
charging_interface_y_pos = case_breite + h_charging_interface - prot;
charging_interface_z_pos = case_hoehe - boost_breite/2 - 98.00;

//Position of tamiya plugs

tamiya_plug_distance = 4.00;
tamiya_x_pos = charging_interface_x_pos  - (tamiya_breite + tamiya_plug_distance/2) + prot;
tamiya_y_pos = case_breite - tamiya_laenge_back_to_bolt;
tamiya_z_pos = charging_interface_z_pos - tamiya_hoehe/2;

//Position of onoff plug

bat_onoff_x_pos = case_laenge*1/6;
bat_onoff_y_pos = case_breite - bat_onoff_kranz_hoehe - bat_onoff_hoehe + prot;
bat_onoff_z_pos = case_hoehe*1/4;

//**************************************//
//POSITIONING OF CONTROL CASE COMPONENTS//
//**************************************//

//Position of Arduino DUE

due_x_pos = case_wall + 2.00;
due_y_pos = case_wall + 2.00;
due_z_pos = case_wall - prot;

//Position of DIN input plug (input from bat or power supply)

controlcase_din_in_x_pos = controlcase_laenge/2 + 10.00;
controlcase_din_in_y_pos = case_wall + prot;
controlcase_din_in_z_pos = controlcase_hoehe/4;

//Position of DIN output plug (output to motor)

controlcase_din_out_x_pos = controlcase_laenge/2 + 10.00;
controlcase_din_out_y_pos = controlcase_breite - case_wall - prot;
controlcase_din_out_z_pos = controlcase_hoehe/2 - 10.00;

//Position of DC coaxial plug on controlcase

controlcase_coax_x_pos = controlcase_laenge/4 + 25.00;
controlcase_coax_y_pos = -prot;
controlcase_coax_z_pos = controlcase_hoehe/4 - breite_dccoax/2;

//Position of DB9 plug (serial interface)

db9_x_pos = controlcase_laenge/4 + 15.00;
db9_y_pos = controlcase_breite;
db9_z_pos = controlcase_hoehe/2 - 10.00;

//Position of OLED display

oled_x_pos = controlcase_laenge*3/4 +10.00;
oled_y_pos = controlcase_breite - case_wall;
oled_z_pos = controlcase_hoehe/2 - 10.00;

//Position of DM556 driver

dm556_x_pos = controlcase_laenge - dm556_laenge + prot;
dm556_y_pos = controlcase_breite/2 - dm556_breite/2;
dm556_z_pos = -prot;

//Position of stepdown converter

sockets_STEPDOWN_x_pos = case_wall + 65.00;
sockets_STEPDOWN_y_pos = case_wall + 25.00;
sockets_STEPDOWN_z_pos = case_wall - prot;

//Position of control case high voltage (motor supply) switch

mot_onoff_x_pos = controlcase_laenge/2 + 10.00 - mot_onoff_kranz_breite/2 ;
mot_onoff_y_pos = - prot;
mot_onoff_z_pos = controlcase_hoehe/2;

//Position of control case low voltage (logic supply) switch

logic_onoff_x_pos = controlcase_laenge/4 + 25.00 - logic_onoff_kranz_breite/2 + breite_dccoax/2;
logic_onoff_y_pos = - prot;
logic_onoff_z_pos = controlcase_hoehe/2;;

//Position of DEBOUNCE unit

sockets_DEBOUNCE_x_pos = case_wall + 25.00;
sockets_DEBOUNCE_y_pos = controlcase_breite - case_wall - DEBOUNCE_y1_shift - 35.00;
sockets_DEBOUNCE_z_pos = case_wall - prot;

// Position of PCB mounting platform

pcb_platform_x_pos = controlcase_laenge - case_wall - pcb_platform_laenge;
pcb_platform_y_pos = case_wall + z;
pcb_platform_z_pos = dm556_hoehe + 3.00;

//Position of driver LED

driver_LED_x_pos = controlcase_laenge*3/4 + oled_display_breite/2 + 10.00 - (driver_LED_laenge + driver_LED_protrusion_laenge)/2;
driver_LED_y_pos = controlcase_breite - (driver_LED_breite + driver_LED_protrusion_breite) + prot;
driver_LED_z_pos = controlcase_hoehe/2 + 10.00;

controller_LED_x_pos = controlcase_laenge*3/4 + oled_display_breite/2 + 10.00 - (controller_LED_laenge + controller_LED_protrusion_laenge)/2;
controller_LED_y_pos = controlcase_breite - case_wall - prot;
controller_LED_z_pos = controlcase_hoehe/2 + 30.00;


//************************************************************************************//
//		MODULES OF CONTROL CASE															//
//************************************************************************************//

//Cutout module for the microcontroller status LED
module controller_LED(controller_LED_x_pos, controller_LED_y_pos, controller_LED_z_pos){
	translate([controller_LED_x_pos, controller_LED_y_pos, controller_LED_z_pos]){
		cube([controller_LED_laenge, controller_LED_breite, controller_LED_hoehe]);
		translate([(controller_LED_laenge - controller_LED_protrusion_laenge)/2, controller_LED_breite, 0]){
			#cube([controller_LED_protrusion_laenge, controller_LED_protrusion_breite, controller_LED_protrusion_hoehe]);
		}
	}
}

//!controller_LED(controller_LED_x_pos, controller_LED_y_pos, controller_LED_z_pos);

//Cutout module for the driver status LED
module driver_LED(driver_LED_x_pos, driver_LED_y_pos, driver_LED_z_pos){
	translate([driver_LED_x_pos, driver_LED_y_pos+prot, driver_LED_z_pos]){
		cube([driver_LED_laenge, driver_LED_breite, driver_LED_hoehe]);
		translate([(driver_LED_laenge - driver_LED_protrusion_laenge)/2, driver_LED_breite - prot ,(driver_LED_hoehe - driver_LED_protrusion_hoehe)/2]){
			cube([driver_LED_protrusion_laenge, driver_LED_protrusion_breite + prot, driver_LED_protrusion_hoehe]);
		}
	}
}

//!driver_LED();

//Cutout module for the Arduino DUE USB interface
module usb_interface(due_x_pos, due_z_pos){
	translate([due_x_pos + 8.50, -prot, due_z_pos + due_feet_hoehe + due_dist_usb_from_bottom - usb_interface_hoehe/2]){
		cube([usb_interface_laenge, case_wall + z, usb_interface_hoehe]);
		translate([usb_interface_dist_ports,0,0]){
			cube([usb_interface_laenge, case_wall + z, usb_interface_hoehe]);
		}
	}
}

//!usb_interface(due_x_pos, due_z_pos);

//Cutout module for the control case high voltage (motor supply) switch
module mot_onoff_switch(mot_onoff_x_pos, mot_onoff_y_pos, mot_onoff_z_pos){
	translate([mot_onoff_x_pos + mot_onoff_breite + (mot_onoff_kranz_breite - mot_onoff_breite)/2, mot_onoff_y_pos + mot_onoff_hoehe + mot_onoff_kranz_hoehe -prot, mot_onoff_z_pos + mot_onoff_laenge + (mot_onoff_kranz_laenge - mot_onoff_laenge)/2]){
		mirror([0,1,0]){
			rotate([-90,90,0]){
				cube([mot_onoff_laenge, mot_onoff_breite, mot_onoff_hoehe]);
				translate([-(mot_onoff_kranz_laenge - mot_onoff_laenge)/2,-(mot_onoff_kranz_breite - mot_onoff_breite)/2, mot_onoff_hoehe -prot]){
					cube([mot_onoff_kranz_laenge, mot_onoff_kranz_breite, mot_onoff_kranz_hoehe]);
				}
			}
		}
	}
}

//!mot_onoff_switch(mot_onoff_x_pos, mot_onoff_y_pos, mot_onoff_z_pos);

//Cutout module for the control case low voltage (logic supply) switch
module logic_onoff_switch(logic_onoff_x_pos, logic_onoff_y_pos, logic_onoff_z_pos){
	translate([logic_onoff_x_pos + logic_onoff_breite + (logic_onoff_kranz_breite - logic_onoff_breite)/2, logic_onoff_y_pos + logic_onoff_hoehe + logic_onoff_kranz_hoehe-prot, logic_onoff_z_pos + logic_onoff_laenge + (logic_onoff_kranz_laenge - logic_onoff_laenge)/2]){
		mirror([0,1,0]){
			rotate([-90,90,0]){
				cube([logic_onoff_laenge, logic_onoff_breite, logic_onoff_hoehe]);
				translate([-(logic_onoff_kranz_laenge - logic_onoff_laenge)/2,-(logic_onoff_kranz_breite - logic_onoff_breite)/2, logic_onoff_hoehe -prot]){
					cube([logic_onoff_kranz_laenge, logic_onoff_kranz_breite, logic_onoff_kranz_hoehe]);
				}
			}
		}
	}
}

//!logic_onoff_switch(logic_onoff_x_pos, logic_onoff_y_pos, logic_onoff_z_pos);

//Cutout module for the basic shape of DM556 motor driver
module dm556_driver_cutout(dm556_x_pos, dm556_y_pos, dm556_z_pos){
	translate([dm556_x_pos, dm556_y_pos, dm556_z_pos]){
		translate([0, (dm556_breite - dm556_pcb_breite)/2, 0]){
			cube([dm556_laenge, dm556_pcb_breite, dm556_hoehe]);
		}
		cube([dm556_laenge, dm556_breite, dm556_kk_hoehe]);
		translate([dm556_laenge - dm556_front_dicke, 0, 0]){
			cube([dm556_front_dicke, dm556_breite, dm556_hoehe]);
		}	
		translate([dm556_laenge + h_m4x16_head, dm556_dist_side_to_screw, dm556_dist_bottom_to_front_screw]){
			rotate([0,90,0]){
				m4_screw();
			}
		}
		translate([dm556_laenge + h_m4x16_head, dm556_breite - dm556_dist_side_to_screw, dm556_dist_bottom_to_front_screw]){
			rotate([0,90,0]){
				m4_screw();
			}
		}
	}
}

//dm556_driver_cutout(0,0,0);

//Holders to attach the DM556 motor driver
module dm556_driver_holder(dm556_x_pos, dm556_y_pos, dm556_z_pos){
	translate([dm556_x_pos + dm556_laenge - dm556_dist_front_to_screw - dm556_holder_laenge/2 , dm556_y_pos - dm556_holder_breite, dm556_z_pos + case_wall - prot]){
		cube([dm556_holder_laenge, dm556_holder_breite, dm556_kk_hoehe - case_wall]);
		translate([0,0,dm556_kk_hoehe - case_wall - prot]){
			difference(){
				cube([dm556_holder_laenge, dm556_holder_breite + dm556_bridge_breite, dm556_bridge_hoehe]);
				translate([dm556_holder_laenge/2, dm556_holder_breite + dm556_dist_side_to_screw, dm556_bridge_hoehe + prot]){
					m4_screw();
				}
			}
		}
		translate([0,dm556_breite + dm556_holder_breite*2,0]){
			mirror([0,1,0]){
				cube([dm556_holder_laenge, dm556_holder_breite, dm556_kk_hoehe - case_wall]);
				
				translate([0,0,dm556_kk_hoehe - case_wall - prot]){
					difference(){
						cube([dm556_holder_laenge, dm556_holder_breite + dm556_bridge_breite, dm556_bridge_hoehe]);
						translate([dm556_holder_laenge/2, dm556_holder_breite + dm556_dist_side_to_screw, dm556_bridge_hoehe + prot]){
							m4_screw();
						}
					}
				}
			}
		}
	}
}

//!dm556_driver_holder(dm556_x_pos, dm556_y_pos, dm556_z_pos);

//Creates the OLED display 
module oled_display(oled_x_pos, oled_y_pos, oled_z_pos){
	translate([oled_x_pos - oled_breite/2, oled_y_pos+prot, oled_z_pos - oled_laenge/2]){
		rotate([-90,-90,0]){
			cube([oled_laenge, oled_breite, case_wall + z - oled_hoehe_screen - oled_screenprotector]);
			translate([oled_dist_screen_from_top, (oled_breite - oled_display_breite)/2, case_wall - prot - oled_hoehe_screen - oled_screenprotector]){
				cube([oled_display_laenge, oled_display_breite, oled_hoehe_screen + oled_screenprotector + z]);
		}
		//Mounting holes
			translate([(oled_laenge - OLED_x_shift)/2, (oled_breite - OLED_y_shift)/2, case_wall + z - oled_hoehe_screen - oled_screenprotector - prot]){
				cylinder(r = d_boardscrew_body/2, h = oled_hoehe_screen);
				translate([OLED_x_shift,0,0]){
					cylinder(r = d_boardscrew_body/2, h = oled_hoehe_screen);
				}
				translate([0,OLED_y_shift,0]){
					cylinder(r = d_boardscrew_body/2, h = oled_hoehe_screen);
				}
				translate([OLED_x_shift,OLED_y_shift,0]){
					cylinder(r = d_boardscrew_body/2, h = oled_hoehe_screen);
				}
			}
		}
	}
}

// oled_display();

//Creates one socket to attach a PCB component with screw
module board_socket(x_shift, y_shift){
	translate([x_shift, y_shift, 0]){
	difference(){
		cylinder(r1 = d_boardscrew_body/2 + boardscrew_socket_wall, r2 = d_boardscrew_body/2 + boardscrew_socket_wall/2, h = h_boardscrew_body);
		translate([0,0, -prot]){
			cylinder(r = d_boardscrew_body/2, h = h_boardscrew_body + z);
		}
	}}
}

//!board_socket();

//Creates LLC screw sockets
module sockets_LLC(){
	board_socket(0,0);
	board_socket(LLC_x_shift, 0);
	board_socket(0, LLC_y_shift);
	board_socket(LLC_x_shift, LLC_y_shift);
}

//!sockets_LLC();

//Creates UNO screw sockets
module sockets_UNO(){
	board_socket(0,0);
	board_socket(UNO_x_shift, 0);
	board_socket(0, UNO_y_shift);
	board_socket(UNO_x_shift, UNO_y_shift);
}

//!sockets_UNO();

//Creates switch debounce module sockets
module sockets_DEBOUNCE(){
	board_socket(0,0);
	board_socket(DEBOUNCE_x_shift, 0);
	board_socket(0, DEBOUNCE_y1_shift);
	board_socket(DEBOUNCE_x_shift, DEBOUNCE_y2_shift);
}

//!sockets_DEBOUNCE();

//Creates Step-Down converter sockets
module sockets_STEPDOWN(){
	board_socket(0,0);
	board_socket(STEPDOWN_x_shift, 0);
	board_socket(0, STEPDOWN_y_shift);
	board_socket(STEPDOWN_x_shift, STEPDOWN_y_shift);
}

//!sockets_STEPDOWN();

//Creates driver llc interface screw sockets
module sockets_DRIVER_INT(){
	board_socket(0,0);
	board_socket(DRIVER_INT_x_shift, 0);
	//board_socket(0, STEPDOWN_y_shift);
	board_socket(DRIVER_INT_x_shift, DRIVER_INT_y2_shift);
}

//!sockets_DRIVER_INT();

module serial_plug(){
	translate([0,0, db9_interface_tiefe - z]){
		cylinder( r = d_db9_interface/2, h = case_wall - db9_interface_tiefe + z + prot);
	}
	translate([-18.50, -7.80, - prot]){
		scale([1,1,db9_interface_tiefe + prot]){
			difference(){
				cube([35.00, 15.00, 2.00]);
				translate([3.20,14.00,0]){
					import ("DB9Mount.stl");
				}
			}
		}
	}	
}

//!serial_plug();

//Second level for PCB mounting
module platform(){	
	difference(){
		difference(){
			difference(){
				cube([pcb_platform_laenge, pcb_platform_breite, pcb_platform_hoehe]);
				/*
				translate([(pcb_platform_laenge - pcb_platform_cutout_laenge)/2, - prot, - prot]){
					cube([pcb_platform_cutout_laenge, pcb_platform_cutout_breite, pcb_platform_hoehe + z]);
				}
				*/
			}
			translate([(pcb_platform_laenge - pcb_platform_cutout_laenge)/2, pcb_platform_breite - pcb_platform_cutout_breite + prot, - prot]){
				cube([pcb_platform_cutout_laenge, pcb_platform_cutout_breite, pcb_platform_hoehe + z]);
			}
		}	
		platform_screws(pcb_platform_hoehe + prot);
	}

	//Place PCB sockets for custom UNO board
	translate([(d_boardscrew_body/2 + boardscrew_socket_wall), pcb_platform_breite - pcb_platform_cutout_breite - (d_boardscrew_body/2 + boardscrew_socket_wall), pcb_platform_hoehe - prot]){
		rotate([0,0,-90]){
			sockets_UNO();
		}
	}

	//Place PCB sockets for LLC board
	translate([(d_boardscrew_body/2 + boardscrew_socket_wall), pcb_platform_breite - pcb_platform_cutout_breite - UNO_x_shift - LLC_y_shift - (d_boardscrew_body/2 + boardscrew_socket_wall)*2 - pcb_platform_board_spacing , pcb_platform_hoehe - prot]){
		rotate([0,0, 0]){
			sockets_LLC();
		}
	}
	//Place PCB sockets for driver interface (DRIVER_INT) board
	translate([(d_boardscrew_body/2 + boardscrew_socket_wall), pcb_platform_breite - pcb_platform_cutout_breite - UNO_x_shift - LLC_y_shift - DRIVER_INT_y2_shift - (d_boardscrew_body/2 + boardscrew_socket_wall)*3 - pcb_platform_board_spacing*2 , pcb_platform_hoehe - prot]){
		rotate([0,0, 0]){
			sockets_DRIVER_INT();
		}
	}
}

//!platform();

//Screw pattern for PCB mounting platform
module platform_screws(screw_z_shift){
	translate([0, 0, screw_z_shift]){
		translate([pcb_platform_screw_offset, pcb_platform_screw_offset, 0]){ 
			m4_screw();
		}
		translate([pcb_platform_laenge - pcb_platform_screw_offset, pcb_platform_screw_offset, 0]){ 
			m4_screw();
		}
		translate([pcb_platform_screw_offset, pcb_platform_breite - pcb_platform_screw_offset, 0]){ 
			m4_screw();
		}
		translate([pcb_platform_laenge - pcb_platform_screw_offset, pcb_platform_breite - pcb_platform_screw_offset, 0]){ 
			m4_screw();
		}	
	}
}

//platform_screws();

module platform_holder_base(){
	cube([(pcb_platform_laenge - pcb_platform_cutout_laenge)/2, pcb_platform_holder_breite, pcb_platform_z_pos - case_wall + prot]);
	translate([0, pcb_platform_breite - pcb_platform_holder_breite, 0]){
		cube([(pcb_platform_laenge - pcb_platform_cutout_laenge)/2, pcb_platform_holder_breite, pcb_platform_z_pos - case_wall + prot]);
	}
	translate([pcb_platform_laenge - (pcb_platform_laenge - pcb_platform_cutout_laenge)/2, pcb_platform_breite - pcb_platform_holder_breite, 0]){
		cube([(pcb_platform_laenge - pcb_platform_cutout_laenge)/2, pcb_platform_holder_breite, pcb_platform_z_pos - case_wall + prot]);
	}
	translate([pcb_platform_laenge - (pcb_platform_laenge - pcb_platform_cutout_laenge)/2, 0, 0]){
		cube([(pcb_platform_laenge - pcb_platform_cutout_laenge)/2, pcb_platform_holder_breite, pcb_platform_z_pos - case_wall + prot]);
	}
}

//platform_holder_base();

module platform_holder_complete(){
	difference(){
		platform_holder_base();
		translate([0,0,h_m4x16_head + prot]){
			platform_screws(pcb_platform_z_pos - case_wall + prot);
		}
	}
}

//!platform_holder_complete();

//Actually just a wrapper...
module controlcase_feet(male_or_female){
	case_feet(male_or_female);
}


//Creating the basic shape control box
module controlcase_base(){
	translate([case_rounding/2, case_rounding/2, 0]){
		minkowski(){
			cube([controlcase_laenge_pre_minkowski, controlcase_breite_pre_minkowski, controlcase_hoehe]);
			cylinder(r = case_rounding/2, h = prot/4);
		}
	}
}

//Collection of cutouts for the control box
module controlcase_cutout(){
	//Thats the main cutout which forms the basic shell 
	translate([case_wall, case_wall, case_wall + prot]){
			cube([controlcase_laenge - case_wall*2, controlcase_breite - case_wall*2, controlcase_hoehe- case_wall]);
	}

	//Cutout the front DIN PLUG connecting the motor
	translate([controlcase_din_in_x_pos, controlcase_din_in_y_pos, controlcase_din_in_z_pos]){
		rotate([90,180,0]){
			din_plug();
		}
	}

	//Cutout the front DIN PLUG connecting the motor
	translate([controlcase_din_out_x_pos, controlcase_din_out_y_pos, controlcase_din_out_z_pos]){
		rotate([-90,0,0]){
			din_plug();
		}
	}

	//Cutout for the DB9 connector on front (control signal for motor)
	translate([db9_x_pos, db9_y_pos, db9_z_pos]){
		rotate([90,0,0]){
			serial_plug();
		}
	}	

	//Cutout the coaxial power plug
	translate([controlcase_coax_x_pos, controlcase_coax_y_pos, controlcase_coax_z_pos]){
		rotate([0,90,0]){
			mirror([0,1,0]){
				dccoax();	
			}
		}
	}
	//Cutout lateral cooling fins
	translate([case_wall+1 -prot, controlcase_breite/2 + 2 , controlcase_hoehe/4]){
		rotate([0,-90,0]){
			cooling_fins(fin_length = 50.00, fin_width = 2.00, fin_dist = 4.00, fin_tilt = 20.00, target_length = controlcase_breite - case_wall*2, target_depth = case_wall+2);
		}
	}

	//Cutout bottom cooling fins

	translate([controlcase_laenge/4 + 25.00, controlcase_breite/2 + 26.00, -case_wall/2]){
			rotate([0,0,90]){
				cooling_fins(fin_length = controlcase_breite/4, fin_width = 2.00, fin_dist = 4.00, fin_tilt = 20.00, target_length = controlcase_laenge/2 - 30.00, target_depth = case_wall*2);
			}
		}

	//Cutout the oled display
	oled_display(oled_x_pos, oled_y_pos, oled_z_pos);

	//Cutout the motor driver status LEDs
	driver_LED(driver_LED_x_pos, driver_LED_y_pos, driver_LED_z_pos);

	//Cutout the controller status LED
	controller_LED(controller_LED_x_pos, controller_LED_y_pos, controller_LED_z_pos);

	//Cutout the DM556 driver cartridge
	dm556_driver_cutout(dm556_x_pos, dm556_y_pos, dm556_z_pos);

	//Cutout the control case high voltage (motor supply) switch
	mot_onoff_switch(mot_onoff_x_pos, mot_onoff_y_pos, mot_onoff_z_pos);

	//Cutout module for the control case low voltage (logic supply) switch
	logic_onoff_switch(logic_onoff_x_pos, logic_onoff_y_pos, logic_onoff_z_pos);

	//Cutout the USB interface
	usb_interface(due_x_pos, due_z_pos);

	//Cutout the holes for the snapper covers
	controlcase_snapper_cutout();

	//Cutout a hole to ease lifting the lid with the finger
	controlcase_lift_hole(zpos = controlcase_hoehe);

}

module controlcase_complete(){
	difference(){
		controlcase_base();
		controlcase_cutout();
	}
	
	//Create the feet
	translate([0,0, -case_feet_hoehe + prot]){
		controlcase_feet(case_feet_male);
	}

	//Create socket for Arduino DUE
	translate([due_x_pos, due_y_pos, due_z_pos]){
		standoffs(boardType = DUE, height = due_feet_hoehe, holeRadius = d_m3x10_body/2, mountType=TAPHOLE);
		
		/* Renders a model of the Arduino DUE */
		/*
		translate([0,0,7]){
			arduino(DUE); // Just for space planning
		}
		*/
	} 
	//Create holders for the DM556 motor driver
	dm556_driver_holder(dm556_x_pos, dm556_y_pos, dm556_z_pos);

	//Create holder for the pcb platform
	translate([pcb_platform_x_pos, pcb_platform_y_pos, case_wall - prot]){
		platform_holder_complete();
	}

	//Create stepdown converter socket
	translate([sockets_STEPDOWN_x_pos,sockets_STEPDOWN_y_pos,sockets_STEPDOWN_z_pos]){
		mirror([1,0,0]){
			rotate([0,0,90]){
				sockets_STEPDOWN();
			}
		}
	}

	//Create socket for debounce unit
	translate([sockets_DEBOUNCE_x_pos,sockets_DEBOUNCE_y_pos,sockets_DEBOUNCE_z_pos]){
		mirror([0,0,0]){
			rotate([0,0,90]){
				sockets_DEBOUNCE();
			}
		}
	}
}

//************************************************************************************//
//		MODULES OF CONTROL CASE TOP LID													//
//************************************************************************************//

//Basic shape of the controlcase top lid
module controlcase_top_base(){
	translate([case_rounding/2, case_rounding/2, 0]){
		minkowski(){
			cube([case_laenge_pre_minkowski, case_breite_pre_minkowski, controlcase_top_hoehe]);
				cylinder(r = case_rounding/2, h = prot/4);
			
		}
	}
	//Add snappers
	controlcase_top_snapper();
}

//!controlcase_top();

//Build one snapper module
module snapper(){
	difference(){
		cube([snapper_laenge, snapper_breite, snapper_hoehe + prot]);
		translate([-prot, -prot, -prot]){
			difference(){
				cube([snapper_laenge + z, snapper_breite + z, snapper_laenge/2]);
				translate([snapper_laenge/2 + prot, snapper_breite + z + prot, snapper_laenge/2]){
					rotate([90,0,0]){
						cylinder(r=snapper_laenge/2, h = snapper_breite + 2*z);
					}
				}
			}
		}
	}
	translate([snapper_laenge/2,0,snapper_laenge/2]){
		scale([1,0.30,1]){
			sphere(r = snapper_laenge/2);
		}
	}
}

//!snapper();

//Build a cutout pattern of snappers for the controlcase
module controlcase_snapper_cutout(){
	translate([0,0,controlcase_hoehe - snapper_hoehe + snapper_laenge/2]){
		for(z = [controlcase_laenge/4, controlcase_laenge*3/4]){
			for(y =[case_wall, controlcase_breite - case_wall]){
				translate([0, y, 0]){
					translate([z,0,0]){	
						scale([1,0.30,1]){
							sphere(r = snapper_laenge/2);
						}
					}
				}
			}
		}
	}
}

//!controlcase_snapper_cutout();

//Position 4 snapper for the controlcase top cover
module controlcase_top_snapper(){
	translate([0,0, -snapper_hoehe + prot]){
		for(z = [controlcase_laenge/4, controlcase_laenge*3/4]){
			//Backfacing
			translate([0,case_wall + snapper_looseness,0]){
				translate([z - snapper_laenge/2,0,0]){
					snapper();
				}	
			}
			//Frontfacing
			translate([0,controlcase_breite - case_wall - snapper_looseness]){
				mirror([0,1,0]){
					translate([z - snapper_laenge/2,0,0]){
						snapper();
					}	
				}
			}
		}
	}
}

//!controlcase_top_snapper();

module controlcase_lift_hole(zpos){
	translate([0, controlcase_breite/2 - controlcase_lift_hole_breite/2, zpos]){
		rotate([-90,0,0]){
			cylinder(r = case_wall/2,	controlcase_lift_hole_breite);
		}
	}
}
//!controlcase_lift_hole(zpos);

//Creates a brim on the lid which secures it in position
module controlcase_top_brim(){
	translate([case_wall + prot, case_wall + prot, -controlcase_brim_hoehe + prot]){
		difference(){
			cube([controlcase_laenge - case_wall*2 - z, controlcase_breite - case_wall*2 - z, controlcase_brim_hoehe + prot]);
			translate([controlcase_brim_breite, controlcase_brim_breite, - prot]){
				cube([controlcase_laenge - case_wall*2 - z - controlcase_brim_breite*2, controlcase_breite - case_wall*2 - z - controlcase_brim_breite*2, controlcase_brim_hoehe + prot + z]);
			}
		}
	}
}

//!controlcase_top_brim();

//Collection of cutouts for the controlcase top lid
module controlcase_top_cutouts(){
	//Cutout some cooling fins
	for(z=[controlcase_breite/4 - 7.50, controlcase_breite/2 + 7.50]){
		translate([controlcase_laenge/2 - 4.00, z, -case_wall/2]){
			rotate([0,0,90]){
				cooling_fins(fin_length = controlcase_breite/4, fin_width = 2.00, fin_dist = 4.00, fin_tilt = 20.00, target_length = controlcase_laenge - case_wall*4 - controlcase_brim_breite*2, target_depth = case_wall*2);
			}
		}
	}
	
	//Cutout a hole to ease lifting the lid with the finger
	controlcase_lift_hole(zpos = 0);
}

//!controlcase_top_cutouts();

//This renders the complete controlcase top lid with all features
module controlcase_top_complete(){
	difference(){
		controlcase_top_base();
		controlcase_top_cutouts();
	}
	controlcase_top_brim();
}

//!controlcase_top_complete();


//************************************************************************************//
//		MODULES OF BATTERY CASE															//
//************************************************************************************//


module bat_onoff_switch(bat_onoff_x_pos, bat_onoff_y_pos, bat_onoff_z_pos){
	translate([bat_onoff_x_pos + bat_onoff_breite + (bat_onoff_kranz_breite - bat_onoff_breite)/2, bat_onoff_y_pos, bat_onoff_z_pos + bat_onoff_laenge + (bat_onoff_kranz_laenge - bat_onoff_laenge)/2]){
		rotate([-90,90,0]){
			cube([bat_onoff_laenge, bat_onoff_breite, bat_onoff_hoehe]);
			translate([-(bat_onoff_kranz_laenge - bat_onoff_laenge)/2,-(bat_onoff_kranz_breite - bat_onoff_breite)/2,bat_onoff_hoehe -prot]){
				cube([bat_onoff_kranz_laenge, bat_onoff_kranz_breite, bat_onoff_kranz_hoehe+prot]);
			}
		}
	}
}

//!bat_onoff_switch(bat_onoff_x_pos, bat_onoff_y_pos, bat_onoff_z_pos);

//Create a charging interface which can be closed by a tight seal 
module charging_interface(charging_interface_x_pos, charging_interface_y_pos, charging_interface_z_pos){
	translate([charging_interface_x_pos, charging_interface_y_pos, charging_interface_z_pos]){
		rotate([90,0,0]){
			difference(){
				cylinder(r = d_charging_interface/2, h = h_charging_interface);
                translate([0,0,-prot]){
                        cylinder(r = (d_charging_interface/2) - charger_wall_breite, h = h_charging_interface+prot);
                }
				
			}
		}
	}		
}

//!charging_interface();

//Creates a tamiya power plug
module tamiya_plug(){
    translate([-10,0,0])
    cube([tamiya_laenge_ges+30, tamiya_breite, tamiya_hoehe]);
    
	translate([tamiya_laenge_back_to_bolt - tamiya_bolt_top_laenge-prot, tamiya_breite/2 - tamiya_bolt_top_breite/2, tamiya_hoehe - prot]){
		cube([tamiya_bolt_top_laenge+2*prot, tamiya_bolt_top_breite, tamiya_bolt_top_hoehe]);
	}
	translate([tamiya_laenge_back_to_bolt - tamiya_bolt_bottom_laenge, 0, -tamiya_bolt_bottom_hoehe + prot]){
		cube([tamiya_bolt_bottom_laenge+prot, tamiya_bolt_bottom_breite, tamiya_bolt_bottom_hoehe]);
	}
	translate([tamiya_laenge_back_to_bolt - tamiya_bolt_bottom_laenge, tamiya_breite - tamiya_bolt_bottom_breite, -tamiya_bolt_bottom_hoehe + prot]){
		cube([tamiya_bolt_bottom_laenge+prot, tamiya_bolt_bottom_breite, tamiya_bolt_bottom_hoehe]);
	}

}

//!tamiya_plug();

//Creates two tamiya power plugs with position and distance between plugs
module tamiya_plugs(tamiya_plug_distance, tamiya_x_pos, tamiya_y_pos, tamiya_z_pos){
	translate([tamiya_breite*2 + tamiya_plug_distance + tamiya_x_pos, tamiya_y_pos, tamiya_z_pos]){
		rotate([0, 0, 90]){
			tamiya_plug();
			translate([0, tamiya_breite + tamiya_plug_distance, 0]){
				tamiya_plug();
			}
		}
	}
}

//!tamiya_plugs(tamiya_plug_distance, tamiya_x_pos, tamiya_y_pos, tamiya_z_pos);

//Creates a coaxial dc plug
module dccoax(){
	translate([-hoehe_dccoax/2,prot,breite_dccoax]){
		rotate([-90,0,-90]){
			if (h_coax_interface <= 0){
				difference(){
					cube([laenge_dccoax_ges+laenge_dcstopper_offset, breite_dccoax, hoehe_dccoax]);
					cube([laenge_dcstopper_offset, breite_dccoax, hoehe_dcstopper_offset]);
				}
			}
			else {
				translate([laenge_dccoax_ges+laenge_dcstopper_offset + h_coax_interface/2 - prot,breite_dccoax/2,hoehe_dccoax/2]){
					rotate([0,90,0]){
						cylinder(r = d_coax_interface/2, h = h_coax_interface + z, center = true);
					}
				}
				difference(){
					cube([laenge_dccoax_ges+laenge_dcstopper_offset, breite_dccoax, hoehe_dccoax]);
					cube([laenge_dcstopper_offset, breite_dccoax, hoehe_dcstopper_offset]);
				}
			}
		}
	}
}		

//!dccoax();

//Creates radiator holes for cooling
module cooling_fins(fin_length, fin_width, fin_dist, fin_tilt, target_length, target_depth){
/*
translate([-2.00, 0, 0]){
	cube([2.00, target_length, 2.00]);
}
*/
	translate([0,-target_length/2 + fin_width,0]){
		for (i = [0:fin_dist:(target_length-fin_width)]){
			translate([0,i,0]){
				rotate([fin_tilt,0,0]){
					hull(){
						translate([fin_width/2,0,0]){
							cylinder(r = fin_width/2, target_depth + z);
							translate([fin_length - fin_width,0,0]){
									cylinder( r = fin_width/2, target_depth + z);
							}
						}
					}	
				}
			}
		}
	}
}

//!cooling_fins(10.00, 1.00, 1.00, 10.00, 15.00, 5.00);

//Creates a DIN Plug for LiYCY cable
module din_plug(){
	translate([0,0, case_wall - din_interface_tiefe]){
		cylinder(r = d_din/2, h = din_interface_tiefe + z);
	}
	cylinder(r = d_din_interface/2, h = case_wall - din_interface_tiefe + prot);		
	
	translate([-din_nose_laenge/2, d_din/2 - 1.00 , case_wall - din_nose_hoehe + z]){
		cube([din_nose_laenge, din_nose_breite + 1.00, din_nose_hoehe]);
	}
}


// One M4x16 screw
module m4_screw(){
	translate([0,0,-(h_m4x16_body + h_m4x16_head - prot)]){
		translate([0,0, h_m4x16_body - prot]){
			cylinder( r = d_m4x16_head/2, h = h_m4x16_head);
		}
		cylinder( r = d_m4x16_body/2, h = h_m4x16_body);
	}
}


//One Multipower MP7.2-12 PB Lead Acid Cell
module battery(){
	
	//Body of battery
	cube([bat_laenge, bat_breite, bat_hoehe]);
	
	//Faston Connector GND
	translate([bat_offset_connectors_front, bat_offset_connectors_side, bat_hoehe-prot]){
		cube([bat_laenge_connectors+z, bat_breite_connectors+z, bat_hoehe_connectors+z]);
	}
	//Faston Connector VCC
	translate([bat_offset_connectors_front, bat_breite - (bat_offset_connectors_side + bat_breite_connectors), bat_hoehe-prot]){
		cube([bat_laenge_connectors + z, bat_breite_connectors + z, bat_hoehe_connectors + z]);
	}
	
}

// Raw block model of boost converter for layouting
module converter(){
	cube([boost_laenge, boost_breite, boost_hoehe]);
}

//Cutout for the boost converter
module boost_cutout(boost_x_pos, boost_y_pos, boost_z_pos){
	translate([boost_x_pos - boost_laenge/2, boost_y_pos+prot, boost_z_pos - boost_breite]){
		rotate([90,0,0]){
			cube([boost_laenge, boost_breite, case_wall+prot + z]);
		}
	}
}

//Holders for the boost converter
module boost_holder(boost_x_pos, boost_y_pos,boost_z_pos){
	translate([boost_x_pos - boost_laenge/2 - case_wall, boost_y_pos + boost_fins_laenge - prot, boost_z_pos - boost_breite]){
		rotate([90,0,0]){
			difference(){
				cube([case_wall, boost_breite, boost_fins_laenge]);
				translate([- prot,boost_breite/2, boost_fins_laenge/2]){
					rotate([0,-90,0]){
						m4_screw();
					}
				}
			}
			difference(){
				translate([boost_laenge + case_wall,0,0]){
					cube([case_wall, boost_breite, boost_fins_laenge]);
				}
				translate([boost_laenge + case_wall*2 + prot,boost_breite/2, boost_fins_laenge/2]){
					rotate([0,90,0]){
						m4_screw();
					}
				}
			}
		}
	}
}

// Two of the cells with spacing
module two_batteries(){
	battery();
	translate([0, bat_breite+tray_wall_breite, 0]){
		battery();
	}
}

module tray_body(){
	cube([tray_laenge, tray_breite, tray_hoehe]);
}

// Cutouts of the tray block
module tray_cutout(){
	
	// Battery space
	translate([tray_wall_breite,tray_wall_breite,tray_wall_breite]){
		scale([1,1,1.5]){t_beam_handle();
		two_batteries();
		}
	}
	// Cooling and access spaces y-dimension
	for (i = [-prot, tray_breite - tray_wall_breite - prot]){
		translate([tray_laenge/3, i, tray_wall_breite]){
			cube([tray_laenge/3, tray_wall_cutout, tray_hoehe]);
		}
	}
	
	//Cooling and access spaces x- dimension
	translate([tray_laenge - tray_wall_breite - prot, tray_breite/4, tray_wall_breite]){
		cube([tray_wall_cutout, tray_breite/2, tray_hoehe]);
	}
	translate([0, tray_breite/4, tray_wall_breite]){
		cube([tray_wall_cutout, tray_breite/2, tray_hoehe]);
	}

	//Cutout mask for cross beam
	translate([tray_laenge*4/6 - z/2, -prot, tray_hoehe - tray_wall_breite]){
		cube([tray_laenge/8 + z, tray_breite + z, tray_wall_breite + z]);
	}
	translate([(tray_laenge/6)*1.25 - z/2, -prot, tray_hoehe - tray_wall_breite]){
		cube([tray_laenge/8 + z, tray_breite + z, tray_wall_breite + z]);
	}
	translate([tray_laenge/3 - z/2, tray_breite/2 -tray_wall_breite/2 - prot, tray_hoehe - tray_wall_breite]){
		cube([tray_laenge/3 + z, tray_wall_breite + z, tray_wall_breite + z]);
	}
	//Cutout the screw holes in the tray
	tray_screws();
	
}

// Beam to hold batteries down. Connect with M4x16mm screws (or M3x16)
module t_beam(tray_x_pos,tray_y_pos,tray_z_pos){
	translate([tray_x_pos,tray_y_pos,tray_z_pos]){
		translate([tray_laenge*4/6, -prot, tray_hoehe - tray_wall_breite]){
			difference(){
				cube([tray_laenge/8, tray_breite, tray_wall_breite]);
				beam_screws();
			}
		}
		translate([(tray_laenge/6)*1.25, -prot, tray_hoehe - tray_wall_breite]){
			difference(){
				cube([tray_laenge/8, tray_breite, tray_wall_breite]);
				beam_screws();
			}
		}
		translate([tray_laenge/3 - prot, tray_breite/3, tray_hoehe - tray_wall_breite]){
			cube([tray_laenge/3 + z, tray_breite/3, tray_wall_breite]);
		}
		//Attach the handle
		t_beam_handle();
	}
}
//t_beam();

//A handle to grab the tray
module t_beam_handle(){
	translate([tray_laenge/3, tray_breite/3, tray_hoehe - prot]){
		cube([tray_wall_breite,tray_breite/3,tray_handle_height + prot]);
		translate([tray_laenge/3  - tray_wall_breite,0,0]){
			cube([tray_wall_breite,tray_breite/3,tray_handle_height + prot]);
		}
		translate([0,0,tray_handle_height - tray_wall_breite + prot]){
			cube([tray_laenge/3, tray_breite/3, tray_wall_breite]);
		}
		reinforce_handle();
	}
}

//Creates reinforcing triangles to stabilize beam handle
module reinforce_handle(){
	translate([-tray_laenge/8, tray_wall_breite, - prot]){
		for( i = [0, tray_breite/3 - tray_wall_breite]){
			translate([0,i,0]){
				rotate([90,0,0]){
					linear_extrude(height = tray_wall_breite){	
						polygon( points = [[0,0], [tray_laenge/8, 0], [tray_laenge/8 + prot, tray_handle_height + prot]]);
					}
				}
			}
		}	
	}
	translate([tray_laenge/8 + tray_laenge/3, tray_breite/3, - prot]){
		rotate([0,0,180]){
			translate([0, tray_wall_breite, 0]){
				for( i = [0, tray_breite/3 - tray_wall_breite]){
					translate([0,i,0]){
						rotate([90,0,0]){
							linear_extrude(height = tray_wall_breite){	
								polygon( points = [[0,0], [tray_laenge/8, 0], [tray_laenge/8 + prot, tray_handle_height + prot]]);
							}
						}
					}
				}	
			}
		}
	}
}

//!reinforce_handle();

module tray_screws(){
//M4 screws on top
	for(i = [(4/6 + 1/16), (1/6 * 1.25 + 1/16)]){
		for(j = [(tray_wall_breite/2), (tray_breite - tray_wall_breite/2), (tray_breite/2)]){
			translate([tray_laenge*i, j, tray_hoehe - (h_m4x16_body + h_m4x16_head) + prot]){
				cylinder(r = d_m4x16_body/2, h = h_m4x16_body + h_m4x16_head - tray_wall_breite);
			}
		}
	}
}

//Describes one M4 screw with head used for the beam
module beam_screw(){
	cylinder(r = d_m4x16_body/2, h = tray_wall_breite - h_m4x16_head + z);
	translate([0,0,tray_wall_breite - h_m4x16_head]){
		cylinder(r = d_m4x16_head/2, h = h_m4x16_head + z);
	}	
}


// Three M4 screw cutouts for the holding beam
module beam_screws(){
	translate([tray_laenge/16, tray_wall_breite/2, 0]){
		beam_screw();
	}

	translate([tray_laenge/16, tray_breite - tray_wall_breite/2, 0]){
		beam_screw();
	}

	translate([tray_laenge/16, tray_breite/2, 0]){
		beam_screw();
	}
	
}

module tray_complete(tray_x_pos, tray_y_pos, tray_z_pos){
	translate([tray_x_pos, tray_y_pos, tray_z_pos]){
		difference(){
			tray_body();
			tray_cutout();
		}
	}
}

//Creates 4 feet at the corners of a case or lid. Can be used as stackable connector.
//If creating male version, it will be slightly smaller to fit.
module case_feet(male_or_female){
	difference(){
		translate([case_rounding/2, case_rounding/2, 0]){
			minkowski(){
				cube([case_laenge_pre_minkowski, case_breite_pre_minkowski, case_feet_hoehe]);
				cylinder(r = case_rounding/2, h = prot/4);
			}
		}
		union(){
			translate([male_or_female, 0 - prot]){
				cube([case_laenge - male_or_female*2, case_breite + z, case_feet_hoehe + z]);
			}
			translate([0, male_or_female, -prot]){
				cube([case_laenge + z, case_breite - male_or_female*2, case_feet_hoehe + z]);
			}
		}
	}
}

//Creates 4 M4x16 screws for every corner of case or lid. Param = Vertical position.
module m4x16_mask_case(z_position){
	translate([case_wall/2 + d_m4x16_body/4, case_wall/2 + d_m4x16_body/4, z_position + prot]){
		m4_screw();
	}
	translate([case_wall/2 + d_m4x16_body/4, case_breite - (case_wall/2 + d_m4x16_body/4), z_position + prot]){
		m4_screw();
	}
	translate([case_laenge - (case_wall/2 + d_m4x16_body/4), case_wall/2 + d_m4x16_body/4, z_position + prot]){
		m4_screw();
	}
	translate([case_laenge - (case_wall/2 + d_m4x16_body/4), case_breite - (case_wall/2 + d_m4x16_body/4), z_position + prot]){
		m4_screw();
	}
}

module case_top_base(){
	translate([case_rounding/2, case_rounding/2, 0]){
		minkowski(){
			cube([case_laenge_pre_minkowski, case_breite_pre_minkowski, case_top_hoehe]);
				cylinder(r = case_rounding/2, h = prot/4);
			
		}
	}
}

module case_top_cutout(){
	//Cutout the feet connectors
	translate([0,0, case_wall - case_top_feet_space]){
		case_feet(case_feet_female);
	}
	
	//Cutout the screw holes
	m4x16_mask_case(case_wall - case_top_feet_space);
	
	//Cutout some cooling fins
	for(z=[case_breite/4 - 7.50, case_breite/2 + 7.50]){
		translate([case_laenge/2 - 4.00, z, -case_wall/2]){
			rotate([0,0,90]){
				cooling_fins(fin_length = case_breite/4, fin_width = 2.00, fin_dist = 4.00, fin_tilt = 20.00, target_length = case_laenge - case_wall*4, target_depth = case_wall*2);
			}
		}
	}
}

//Creates the top lid of the case
module case_top_complete(){
	difference(){
		case_top_base();
		case_top_cutout();
	}
}

//!case_top_complete();

//Creates the base shape of the battery case
module case_base(){
		translate([case_rounding/2, case_rounding/2, 0]){
			minkowski(){
				cube([case_laenge_pre_minkowski, case_breite_pre_minkowski, case_hoehe]);
				cylinder(r = case_rounding/2, h = prot/4);
			}
		}
}

//Collection of cutouts for the battery case
module case_cutout(){
	//Thats the main cutout which forms the basic shell 
	translate([case_wall, case_wall, case_wall + prot]){
			cube([case_laenge - case_wall*2, case_breite - case_wall*2, case_hoehe- case_wall]);
	}

	//Cutout a socket for the battery tray
	translate([case_wall + z, case_wall + z, case_wall - tray_socket_hoehe + prot]){
		cube([tray_laenge + z, tray_breite + z, tray_socket_hoehe + z]);
	}

	//Cutout the holes for the beam lock, first the half penetrating one, then the full one
	translate([case_wall/2 - prot, tray_y_pos + tray_breite*2/3 + z - prot, tray_z_pos + tray_hoehe + z - prot]){
		cube([case_wall/2 + z, beam_lock_breite + z, beam_lock_hoehe + z]);
	}

	translate([case_laenge - case_wall - prot, tray_y_pos + tray_breite*2/3 + z - prot, tray_z_pos + tray_hoehe + z - prot]){
		cube([case_wall + z, beam_lock_breite + z, beam_lock_hoehe*2 + z]);
	}
	
	// Cutout the screw ports on top
	m4x16_mask_case(case_hoehe + case_top_feet_space + prot);

	//Cutout the hole for the booster radiator
	boost_cutout(boost_x_pos, boost_y_pos, boost_z_pos); // Position from top
	
	//Cutout the DIN plug port
	translate([din_x_pos, din_y_pos, din_z_pos]){ 
		rotate([-90,0,0]){
			din_plug();
		}
	}
	
	//Cutout the tamiya plugs
	tamiya_plugs(tamiya_plug_distance, tamiya_x_pos, tamiya_y_pos, tamiya_z_pos);

	//Cutout the coaxial dc/dc plug
	translate([coax_x_pos_bat, coax_y_pos_bat, coax_z_pos_bat]){
		dccoax();	
	}

	//Cutout the on/off switch
	bat_onoff_switch(bat_onoff_x_pos, bat_onoff_y_pos, bat_onoff_z_pos);

	//Cutout some cooling fins on the side
	translate([case_laenge + prot, case_breite/2, case_hoehe/5]){
		rotate([0,-90,0]){
			cooling_fins(fin_length = 50.00, fin_width = 2.00, fin_dist = 4.00, fin_tilt = 20.00, target_length = case_breite - case_wall*2, target_depth = case_wall+2);
		}
	}
	
	//... some on the other side
	translate([case_wall+1 -prot, case_breite/2 +2 , case_hoehe/5]){
		rotate([0,-90,0]){
			cooling_fins(fin_length = 50.00, fin_width = 2.00, fin_dist = 4.00, fin_tilt = 20.00, target_length = case_breite - case_wall*2, target_depth = case_wall+2);
		}
	}

	//...and some more on the front
	translate([case_laenge/2 - 2, case_wall+1 - prot , case_hoehe/5]){
		rotate([0,-90,90]){
			cooling_fins(fin_length = 50.00, fin_width = 2.00, fin_dist = 4.00, fin_tilt = 20.00, target_length = case_breite - case_wall*2, target_depth = case_wall+2);
		}
	}

}

//Creates the complete battery case
module case_complete(){

	//Attaching the charger interface
	charging_interface(charging_interface_x_pos, charging_interface_y_pos, charging_interface_z_pos);
	
	//Attaching "wings" to hold the booster cooler
	boost_holder(boost_x_pos, boost_y_pos, boost_z_pos);
	
	//Attaching a block to hold the beam lock screw
	translate([case_laenge - case_wall*2 + prot, tray_y_pos + tray_breite*2/3 + z - beam_lock_breite/2, tray_z_pos + tray_hoehe + beam_lock_hoehe + z + prot]){
		difference(){
			cube([case_wall + z, beam_lock_breite*2, beam_lock_hoehe]);
			translate([case_wall + h_m4x16_head + z + prot, beam_lock_breite, beam_lock_hoehe/2]){
				rotate([0,90,0]){
					m4_screw();
				}
			}
		}
	}

	//Cutout everything from the case_cutout module collection
	difference(){
		case_base();
		case_cutout();
	}
}

//Creates a locking beam to hold down the battery pack
module beam_lock(beam_lock_x_pos, beam_lock_y_pos, beam_lock_z_pos){
	translate([beam_lock_x_pos, beam_lock_y_pos, beam_lock_z_pos]){
		cube([beam_lock_laenge, beam_lock_breite, beam_lock_hoehe]);
		difference(){
			translate([beam_lock_laenge - case_wall, 0, beam_lock_hoehe - prot]){
				cube([case_wall, beam_lock_breite, beam_lock_hoehe + prot]);
			}
			translate([beam_lock_laenge + prot, beam_lock_breite/2 , beam_lock_hoehe*3/2]){
				rotate([0,90,0]){
					m4_screw();
				}
			}
		}
	}
}

//Creates a rubber protection cap for the charging interface 
module charger_cap(charging_interface_x_pos, charging_interface_y_pos, charging_interface_z_pos){
	translate([charging_interface_x_pos, charging_interface_y_pos, charging_interface_z_pos]){
		rotate([90,0,0]){
			//Creates the grips on the surface of the cap
			for(i = [0 : num_grip]){
				rotate([0,0, i*360/num_grip]){
					translate([d_charger_cap_aussen/2,0,0]){
						cylinder(h = h_charger_cap_ges, r1 = d_grip/4, r2 = d_grip/2, center = true);
					}
				}
			}
			difference(){
				cylinder(h = h_charger_cap_ges, r = d_charger_cap_aussen/2, center = true);
				translate([0,0,h_charger_cap_boden/2 + prot]){
					cylinder(h = h_charger_cap_ges - h_charger_cap_boden, r = (d_charging_interface + z)/2, center = true);
				}
			}
			translate([0,0,-(h_charger_cap_ges/2) + (h_charger_cap_boden*5/3)/2]){
				cylinder(h = h_charger_cap_boden*5/3, r = d_charger_cap_innen/2, center = true);
			}
		}
	}
}


//********************//
// RENDER CONTROL CASE//
//********************//

//translate([0,0,400]){ //Display shift for all control case components
	
	//Render the control case
	//controlcase_complete();
	
	
	//Render the top cover of the controlcase
	//translate([0,0,controlcase_hoehe + 100]){
		//controlcase_top_complete();
	//}

	//Render PCB mounting platform
	//translate([pcb_platform_x_pos, pcb_platform_y_pos, //pcb_platform_z_pos]){  
		//platform();
	//}

	
//}


//********************//
// RENDER BATTERY CASE//
//********************//


//case_complete();

//!translate([0,0,case_hoehe + 100.00]){
//case_top_complete();
//}


//translate([0,0,-case_feet_hoehe + prot]){
	//case_feet(case_feet_male);
//}

//charger_cap(charging_interface_x_pos, charging_interface_y_pos + 150.00, charging_interface_z_pos);


//t_beam(tray_x_pos,tray_y_pos,tray_z_pos);

//tray_complete(tray_x_pos, tray_y_pos, tray_z_pos);

//beam_lock(beam_lock_x_pos, beam_lock_y_pos, beam_lock_z_pos); 






