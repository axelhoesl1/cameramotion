
/*
Library for writing common datatypes to the I2C connected EEPROM Chip 24LC256.
Author:	Patrick Moerwald 
		pjm85@directbox.com
*/

#ifndef I2CEEPROM_h
#define I2CEEPROM_h

#include <stdlib.h>
#if ARDUINO >= 100
#include <Arduino.h>
#else
#include <WProgram.h>
#include <wiring.h>
#endif
#include <Wire.h>


class I2CEEPROM
{
	public:

	I2CEEPROM();

	void write_byte(unsigned int addr, byte data);
	void write_int16_t(unsigned int addr, int16_t data);
	void write_int32_t(unsigned int addr, int32_t data);

	byte read_byte(unsigned int addr);
	int16_t read_int16_t(unsigned int addr);
	int32_t read_int32_t(unsigned int addr);

	private:

	void write(unsigned int addr, byte data);
	byte read(unsigned int addr);

};

extern I2CEEPROM myEEPROM;

#endif
