/*
Library for writing common datatypes to the I2C connected EEPROM Chip 24LC256.
Author:	Patrick Moerwald 
		pjm85@directbox.com
*/


#include "I2CEEPROM.h"


#define EEPROM_ADDR 0x50
#define DEBUG 1 // 0

I2CEEPROM::I2CEEPROM()
{
	Wire.begin();

}

/*
These functions encapsulate basic write functions to write byte, int and long
*/

void I2CEEPROM::write_byte(unsigned int addr, byte data_byte) {
  write(addr, data_byte);
}

void I2CEEPROM::write_int16_t(unsigned int addr, int16_t data_int) {
  write(addr, data_int >> 8 & 0xFF); // Writing LSB
  write(addr+1, data_int & 0xFF); // Writing MSB
}

void I2CEEPROM::write_int32_t(unsigned int addr, int32_t data_long) {
  for (int i = 3; i >= 0; i--) {
    write(addr, (data_long >> 8 * i) & 0xFF);
    addr++;
  }
}

/*
These functions encapsulate basic write functions to write byte, int and long
*/

byte I2CEEPROM::read_byte(unsigned int addr){
  return read(addr);
}

int16_t I2CEEPROM::read_int16_t(unsigned int addr) {
  int16_t result;
  result = (int16_t)(read(addr) << 8) | (int16_t)read(addr + 1); // Reading MSB
  return result;
}

int32_t I2CEEPROM::read_int32_t(unsigned int addr) {
  int32_t result;
    result = (int32_t)(read(addr)) << 24 | (int32_t)(read(addr+1)) << 16 | (int32_t)(read(addr+2)) << 8 | (int32_t)(read(addr+3));
  return result;
}

/*
These two functions help us write to the 24LC256 EEPROM chip
*/

void I2CEEPROM::write(unsigned int addr, byte data) {
  int rdata = data;
  Wire.beginTransmission(EEPROM_ADDR);
  Wire.write((int)(addr >> 8));       // MSB
  Wire.write((int)(addr & 0xFF));     // LSB
  Wire.write(rdata);
  Wire.endTransmission();
  if (DEBUG == 1)
  {
  Serial.print("EEPROM write: addr: ");
  Serial.print(addr);
  Serial.print(" ");
  Serial.println(data);
  }
  delay(10);
}

byte I2CEEPROM::read(unsigned int addr) {
  byte data = 0xFF;
  Wire.beginTransmission(EEPROM_ADDR);
  Wire.write((int)(addr >> 8));       // MSB
  Wire.write((int)(addr & 0xFF));     // LSB
  Wire.endTransmission();
  Wire.requestFrom(EEPROM_ADDR, 1);
  if (Wire.available()) data = Wire.read();
  if (DEBUG == 1)
  {
  Serial.print("EEPROM read: addr: ");
  Serial.print(addr);
  Serial.print(" ");
  Serial.println(data);
  }
  delay(10);
  return data;
}